  <!DOCTYPE HTML>
<html lang="am">
 	<head>
  		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0, viewport-fit=cover"/>
		<meta name="format-detection" content="telephone=no"/>
		<meta name="description" content="Aren Mehrabyan foundation website">
  		<title>ԱՐԵՆ ՄԵՀՐԱԲՅԱՆ ՀԻՄՆԱԴՐԱՄ</title>
		<link rel="stylesheet" href="css/main.css">
		<link rel="stylesheet" href="css/about.css">
		<?php
			include 'templates/favicons.php'
		?>
 	</head>
 	<body>
 		<?php
			include 'templates/header.php'
		?>
 		<div class="content">
			<div class="page_head_block">
				<div class="head_inner">
					<div class="page_container">
						<div class="info_block">
							<h1 class="page_title">Մեր նպատակը</h1>
							<div class="page_description">Հիմնադրամի ստեղծումը նախաձեռնել է Արեն Մեհրաբյանի մեծ ընտանիքը՝ հիմքում դնելով ավագ որդու՝ Արենի երազանքը, այն է` հաստատվել Հայաստանում և գիտության զարգացումը խթանելով՝ հզորացնել հայրենիքը:</div>
						</div>
						<div class="image_block">
							<img src="images/mission_top_image.jpg" alt="" title="" width="1160" height="700"/>
						</div>
					</div>
				</div>
			</div>

			<div class="info_section">
				<div class="page_container">
					<div class="info_block">
						<div class="inner_block">
							<div class="title_block">Ստեղծել</div>
							<div class="text_block">մարդակենտրոն աջակցման համակարգ՝ տրամադրելով համապատասխան կրթություն մասնագիտություն չունեցող, ռազմական գործողությունների հետևանքով հաշմանդամություն ձեռք բերած ու դրա արդյունքում գործազուրկ դարձած անձանց</div>
						</div>
						<div class="inner_block">
							<div class="title_block">Աջակցել</div>
							<div class="text_block">զոհված զինծառայողների ընտանիքների անդամներին՝ մասնագիտանալու ինժեներական, տեխնիկական և այլ նեղ մասնագիտությունների ոլորտում</div>
						</div>
						<div class="inner_block">
							<div class="title_block">Սատարել</div>
							<div class="text_block">հաշմանդամություն ունեցող անձանց սոցիալական ինտեգրմանը կրթության և զբաղվածության ապահովման միջոցով</div>
						</div>
					</div>
					<div class="image_block">
						<img src="images/mission_inner_image.jpg" alt="" title="" width="570" height="590"/>
					</div>
				</div>
			</div>
	
 		</div>
		<?php
			include 'templates/footer.php'
		?>
		<script src="js/jquery-3.6.0.min.js"></script>
	 	<script src="js/main.js"></script>
 	</body>
</html>