<div class="footer">
	<div class="footer_top">
        <div class="page_container">
            <div class="logo_block">
                <img src="css/images/main_logo.svg" alt="" title="" width="213" height="95"/>
            </div>
            <ul class="socials_list">
                <li><a href="https://www.facebook.com" class="icon_facebook" target="_blank">Facebook</a></li>
                <li><a href="https://www.instagram.com" class="icon_instagram" target="_blank">instagram</a></li>
                <li><a href="https://www.linkedin.com" class="icon_linkedin" target="_blank">linkedin</a></li>
            </ul>
        </div>
    </div>
    <div class="footer_middle">
        <div class="page_container">
            <div class="footer_contacts">
                <div class="contact_block">
                    <div class="contact_type">Հասցե</div>
                    <div class="contact_info">Նոր Նորքի 5-րդ զանգված, Ա. Միկոյան 15, Երևան, Հայաստան</div>
                </div>
                <div class="contact_block">
                    <div class="contact_type">Բջջային հեռախոս </div>
                    <div class="contact_info"><a href="tel:+37441107148" class="phone_link">(+374) 41-10-71-48</a></div>
                </div>
                <div class="contact_block">
                    <div class="contact_type">Էլեկտրոնային հասցե</div>
                    <div class="contact_info"><a href="mailto:info@arenmehrabyan.or">info@arenmehrabyan.or</a></div>
                </div>
            </div>
            <div class="footer_menu">
                <ul>
                    <li>
                        <div>Մեր մասին</div>
                        <ul>
                            <li><a href="history.php">Մեր պատմությունը</a></li>
                            <li><a href="mission.php">Մեր նպատակը</a></li>
                            <li><a href="vision.php">Մեր տեսլականը</a></li>
                            <li><a href="team.php">Մեր թիմը</a></li>
                            <li><a href="graduates.php">Մեր շրջանավարտները</a></li>
                        </ul>
                    </li>
                    <li>
                        <div>Մեր ծրագրերը</div>
                        <ul>
                            <li><a href="courses.php">Դասընթացներ</a></li>
                            <li><a href="academy.php">Գիտակրթական ակադեմիա, թանգարան - լաբորատորիա</a></li>
                            <li><a href="aerospace.php">Աերոտիեզերական հայկական ծրագիր</a></li>
                        </ul>
                    </li>
                    <li><a href="cooperate.php">Համագործակցել</a></li>
                    <li><a href="blog.php">Բլոգ</a></li>
                    <li><a href="faq.php">Հաճախ տրվող հարցեր</a></li>
                    <li><a href="contacts.php">Կապ</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer_bottom">
        <div class="page_container">
            <div class="copyrights">Հեղինակային իրավունքները պաշտպանված են 2022</div>
            <div class="developer">Create by 
                <a href="http://phpstack-351614-1719656.cloudwaysapps.com/" target="_blank">
                    <img src="css/images/dev_logo.svg" alt="Inpoint" title="Inpoint" width="62" height="17"/>
                </a>
            </div>
        </div>
    </div>
</div>