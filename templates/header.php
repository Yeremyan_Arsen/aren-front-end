<div class="header">
	<div class="header_inner">
		<div class="page_container">
			<div class="main_logo">
				<?php if( strpos($_SERVER['SCRIPT_FILENAME'], 'index.php') == false):?> <a href="index.php"><?php endif?>
					<img src="css/images/main_logo.svg" alt="" title="" width="193" height="86"/>
				<?php if( strpos($_SERVER['SCRIPT_FILENAME'], 'index.php') == false):?> Արեն Մեհրաբյան հիմնադրամ</a><?php endif?>
			</div>
			<div class="menu_block">
				<div class="menu_inner">
					<ul class="main_menu">
						<li><a href="index.php">Գլխավոր</a></li>
						<li class="<?php if( strpos($_SERVER['SCRIPT_FILENAME'], 'mission.php') == true || strpos($_SERVER['SCRIPT_FILENAME'], 'vision.php') == true || strpos($_SERVER['SCRIPT_FILENAME'], 'team.php') == true || strpos($_SERVER['SCRIPT_FILENAME'], 'history.php') == true || strpos($_SERVER['SCRIPT_FILENAME'], 'graduates.php') == true):?>current<?php endif?>">
							<a href="#" class="submenu_btn">Մեր մասին</a>
							<ul class="submenu_list">
								<li><a href="history.php">Մեր պատմությունը</a></li>
								<li><a href="mission.php">Մեր նպատակը</a></li>
								<li><a href="vision.php">Մեր տեսլականը</a></li>
								<li><a href="team.php">Մեր թիմը</a></li>
								<li><a href="graduates.php">Մեր շրջանավարտները</a></li>
							</ul>
						</li>
						<li class="<?php if( strpos($_SERVER['SCRIPT_FILENAME'], 'cources.php') == true || strpos($_SERVER['SCRIPT_FILENAME'], 'academy.php') == true || strpos($_SERVER['SCRIPT_FILENAME'], 'aerospace.php') == true):?>current<?php endif?>">
							<a href="#" class="submenu_btn">Մեր ծրագրերը</a>
							<ul class="submenu_list">
								<li><a href="courses.php">Դասընթացներ</a></li>
								<li><a href="academy.php">Գիտակրթական ակադեմիա, թանգարան - լաբորատորիա</a></li>
								<li><a href="aerospace.php">Աերոտիեզերական հայկական ծրագիր</a></li>
							</ul>
						</li>
						<li class="<?php if( strpos($_SERVER['SCRIPT_FILENAME'], 'cooperate.php') == true):?>current<?php endif?>"><a href="cooperate.php">Համագործակցել</a></li>
						<li class="<?php if( strpos($_SERVER['SCRIPT_FILENAME'], 'blog.php') == true || strpos($_SERVER['SCRIPT_FILENAME'], 'blog_inner.php') == true):?>current<?php endif?>"><a href="blog.php">Բլոգ</a></li>
						<li><a href="faq.php">ՀՏՀ</a></li>
						<li><a href="contacts.php">Կապ</a></li>
					</ul>
					<div class="header_actions">
						<a href="donate.php" class="donate_btn <?php if( strpos($_SERVER['SCRIPT_FILENAME'], 'donate.php') == true):?>current<?php endif?>">Նվիրաբերել</a>
						<a href="index.php" class="lg_btn">Eng</a>
					</div>
				</div>
			</div>
			<button class="menu_btn" aria-label="menu">
				<span></span>
			</button>
		</div>
		
	</div>
</div>




