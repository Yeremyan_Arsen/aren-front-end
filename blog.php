  <!DOCTYPE HTML>
<html lang="am">
 	<head>
  		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0, viewport-fit=cover"/>
		<meta name="format-detection" content="telephone=no"/>
		<meta name="description" content="Aren Mehrabyan foundation website">
  		<title>ԱՐԵՆ ՄԵՀՐԱԲՅԱՆ ՀԻՄՆԱԴՐԱՄ</title>
		<link rel="stylesheet" href="css/main.css">
		<link rel="stylesheet" href="css/blog.css">
		<?php
			include 'templates/favicons.php'
		?>
 	</head>
 	<body>
 		<?php
			include 'templates/header.php'
		?>
 		<div class="content">
			<div class="page_title_block">
				<div class="page_container">
					<h1 class="page_title">Բլոգ</h1>
				</div>
			</div>
			<div class="listing_section">
				<div class="page_container">
					<ul class="news_list">
						<li>
							<div class="news_block">
								<a href="blog_inner.php" class="image_block">
									<img src="images/news_image1.jpg" alt="" title="" width="375" height="260"/>
									Մարտի 23-ին, 2022թ. մեկնարկեց Ծրագրավորման դասընթացների նոր փուլը
								</a>
								<div class="title_block"><a href="blog_inner.php">Մարտի 23-ին, 2022թ. մեկնարկեց Ծրագրավորման դասընթացների նոր փուլը</a></div>
								<div class="date_time">
									<span>17.03.2021</span>
									<span>3 րոպե</span>
								</div>
							</div>
						</li>
						<li>
							<div class="news_block">
								<a href="blog_inner.php" class="image_block">
									<img src="images/news_image2.jpg" alt="" title="" width="375" height="260"/>
									Տեխնոլոգիական կրթության ակադեմիային (TTA) և FESTO ընկերության լիազորված ներկայացուցիչ Կոմպատեր ՍՊԸ-ին, ինչպես նաև
								</a>
								<div class="title_block"><a href="blog_inner.php">Տեխնոլոգիական կրթության ակադեմիային (TTA) և FESTO ընկերության լիազորված ներկայացուցիչ Կոմպատեր ՍՊԸ-ին, ինչպես նաև</a></div>
								<div class="date_time">
									<span>17.03.2021</span>
									<span>3 րոպե</span>
								</div>
							</div>
						</li>
						<li>
							<div class="news_block">
								<a href="blog_inner.php" class="image_block">
									<img src="images/news_image3.jpg" alt="" title="" width="375" height="260"/>
									Թորոնթոյի Համազգայինի թատերասրահում կներկայացնեմ իմ « Խոստում »
								</a>
								<div class="title_block"><a href="blog_inner.php">Թորոնթոյի Համազգայինի թատերասրահում կներկայացնեմ իմ « Խոստում »</a></div>
								<div class="date_time">
									<span>17.03.2021</span>
									<span>3 րոպե</span>
								</div>
							</div>
						</li>
						<li>
							<div class="news_block">
								<a href="blog_inner.php" class="image_block">
									<img src="images/news_image1.jpg" alt="" title="" width="375" height="260"/>
									Մարտի 23-ին, 2022թ. մեկնարկեց Ծրագրավորման դասընթացների նոր փուլը
								</a>
								<div class="title_block"><a href="blog_inner.php">Մարտի 23-ին, 2022թ. մեկնարկեց Ծրագրավորման դասընթացների նոր փուլը</a></div>
								<div class="date_time">
									<span>17.03.2021</span>
									<span>3 րոպե</span>
								</div>
							</div>
						</li>
						<li>
							<div class="news_block">
								<a href="blog_inner.php" class="image_block">
									<img src="images/news_image1.jpg" alt="" title="" width="375" height="260"/>
									Մարտի 23-ին, 2022թ. մեկնարկեց Ծրագրավորման դասընթացների նոր փուլը
								</a>
								<div class="title_block"><a href="blog_inner.php">Մարտի 23-ին, 2022թ. մեկնարկեց Ծրագրավորման դասընթացների նոր փուլը</a></div>
								<div class="date_time">
									<span>17.03.2021</span>
									<span>3 րոպե</span>
								</div>
							</div>
						</li>
						<li>
							<div class="news_block">
								<a href="blog_inner.php" class="image_block">
									<img src="images/news_image2.jpg" alt="" title="" width="375" height="260"/>
									Տեխնոլոգիական կրթության ակադեմիային (TTA) և FESTO ընկերության լիազորված ներկայացուցիչ Կոմպատեր ՍՊԸ-ին, ինչպես նաև
								</a>
								<div class="title_block"><a href="blog_inner.php">Տեխնոլոգիական կրթության ակադեմիային (TTA) և FESTO ընկերության լիազորված ներկայացուցիչ Կոմպատեր ՍՊԸ-ին, ինչպես նաև</a></div>
								<div class="date_time">
									<span>17.03.2021</span>
									<span>3 րոպե</span>
								</div>
							</div>
						</li>
						<li>
							<div class="news_block">
								<a href="blog_inner.php" class="image_block">
									<img src="images/news_image3.jpg" alt="" title="" width="375" height="260"/>
									Թորոնթոյի Համազգայինի թատերասրահում կներկայացնեմ իմ « Խոստում »
								</a>
								<div class="title_block"><a href="blog_inner.php">Թորոնթոյի Համազգայինի թատերասրահում կներկայացնեմ իմ « Խոստում »</a></div>
								<div class="date_time">
									<span>17.03.2021</span>
									<span>3 րոպե</span>
								</div>
							</div>
						</li>
						<li>
							<div class="news_block">
								<a href="blog_inner.php" class="image_block">
									<img src="images/news_image1.jpg" alt="" title="" width="375" height="260"/>
									Մարտի 23-ին, 2022թ. մեկնարկեց Ծրագրավորման դասընթացների նոր փուլը
								</a>
								<div class="title_block"><a href="blog_inner.php">Մարտի 23-ին, 2022թ. մեկնարկեց Ծրագրավորման դասընթացների նոր փուլը</a></div>
								<div class="date_time">
									<span>17.03.2021</span>
									<span>3 րոպե</span>
								</div>
							</div>
						</li>
						<li>
							<div class="news_block">
								<a href="blog_inner.php" class="image_block">
									<img src="images/news_image1.jpg" alt="" title="" width="375" height="260"/>
									Մարտի 23-ին, 2022թ. մեկնարկեց Ծրագրավորման դասընթացների նոր փուլը
								</a>
								<div class="title_block"><a href="blog_inner.php">Մարտի 23-ին, 2022թ. մեկնարկեց Ծրագրավորման դասընթացների նոր փուլը</a></div>
								<div class="date_time">
									<span>17.03.2021</span>
									<span>3 րոպե</span>
								</div>
							</div>
						</li>
						<li>
							<div class="news_block">
								<a href="blog_inner.php" class="image_block">
									<img src="images/news_image2.jpg" alt="" title="" width="375" height="260"/>
									Տեխնոլոգիական կրթության ակադեմիային (TTA) և FESTO ընկերության լիազորված ներկայացուցիչ Կոմպատեր ՍՊԸ-ին, ինչպես նաև
								</a>
								<div class="title_block"><a href="blog_inner.php">Տեխնոլոգիական կրթության ակադեմիային (TTA) և FESTO ընկերության լիազորված ներկայացուցիչ Կոմպատեր ՍՊԸ-ին, ինչպես նաև</a></div>
								<div class="date_time">
									<span>17.03.2021</span>
									<span>3 րոպե</span>
								</div>
							</div>
						</li>
						<li>
							<div class="news_block">
								<a href="blog_inner.php" class="image_block">
									<img src="images/news_image3.jpg" alt="" title="" width="375" height="260"/>
									Թորոնթոյի Համազգայինի թատերասրահում կներկայացնեմ իմ « Խոստում »
								</a>
								<div class="title_block"><a href="blog_inner.php">Թորոնթոյի Համազգայինի թատերասրահում կներկայացնեմ իմ « Խոստում »</a></div>
								<div class="date_time">
									<span>17.03.2021</span>
									<span>3 րոպե</span>
								</div>
							</div>
						</li>
						<li>
							<div class="news_block">
								<a href="blog_inner.php" class="image_block">
									<img src="images/news_image1.jpg" alt="" title="" width="375" height="260"/>
									Մարտի 23-ին, 2022թ. մեկնարկեց Ծրագրավորման դասընթացների նոր փուլը
								</a>
								<div class="title_block"><a href="blog_inner.php">Մարտի 23-ին, 2022թ. մեկնարկեց Ծրագրավորման դասընթացների նոր փուլը</a></div>
								<div class="date_time">
									<span>17.03.2021</span>
									<span>3 րոպե</span>
								</div>
							</div>
						</li>
					</ul>
					<div class="paging">
						<ul>
							<li><a href="#" class="icon_left prev_page inactive">Previous</a></li>
							<li><a href="#" class="current_page">1</a></li>
							<li><a href="#">2</a></li>
							<li><a href="#">3</a></li>
							<li>...</li>
							<li><a href="#">13</a></li>
							<li><a href="#" class="icon_right next_page">Next</a></li>
						</ul>
					</div>
	
				</div>
			</div>
 		</div>
		<?php
			include 'templates/footer.php'
		?>
		<script src="js/jquery-3.6.0.min.js"></script>
	 	<script src="js/main.js"></script>
 	</body>
</html>