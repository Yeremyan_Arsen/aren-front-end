  <!DOCTYPE HTML>
<html lang="am">
 	<head>
  		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0, viewport-fit=cover"/>
		<meta name="format-detection" content="telephone=no"/>
		<meta name="description" content="Aren Mehrabyan foundation website">
  		<title>ԱՐԵՆ ՄԵՀՐԱԲՅԱՆ ՀԻՄՆԱԴՐԱՄ</title>
		<link rel="stylesheet" href="css/main.css">
		<link rel="stylesheet" href="css/about.css">
		<?php
			include 'templates/favicons.php'
		?>
 	</head>
 	<body>
 		<?php
			include 'templates/header.php'
		?>
 		<div class="content">
			 <div class="team_section">
				 <div class="page_container">
					<div class="section_head">
						<h1 class="page_title">Մեր Թիմը</h1>
					 </div>
					 <div class="members_list">
						 <ul>
							 <li>
								 <div class="member_image">
									 <img src="images/member1.jpg" alt="" title="" width="170" height="170"/>
								 </div>
								 <div class="member_name">Դավթյան Արման</div>
								 <div class="member_post">Գործադիր տնօրեն</div>
							 </li>
							 <li>
								<div class="member_image">
									<img src="images/member2.jpg" alt="" title="" width="170" height="170"/>
								</div>
								<div class="member_name">Արմինե Հովհաննիսյան</div>
								<div class="member_post">Զարգացման գծով տնօրեն</div>
							</li>
							<li>
								<div class="member_image">
									<img src="images/member3.jpg" alt="" title="" width="170" height="170"/>
								</div>
								<div class="member_name">Հովհաննես Աբրահամյան</div>
								<div class="member_post">Հիմնադրի ներկայացուցիչ, ավագ խորհրդական</div>
							</li>
							<li>
								<div class="member_image">
									<img src="images/member4.jpg" alt="" title="" width="170" height="170"/>
								</div>
								<div class="member_name">Մուրադյան Բաբկեն</div>
								<div class="member_post">Զարգացման բաժնի ղեկավար</div>
							</li>
							<li>
								<div class="member_image">
									<img src="images/member5.jpg" alt="" title="" width="170" height="170"/>
								</div>
								<div class="member_name">Սոնա Նիկողոսյան</div>
								<div class="member_post">Խորհրդատու</div>
							</li>
							<li>
								<div class="member_image">
									<img src="images/member6.jpg" alt="" title="" width="170" height="170"/>
								</div>
								<div class="member_name">Կարեն Ղազարյան</div>
								<div class="member_post">Ֆինանսական բաժնի ղեկավար</div>
							</li>
							<li>
								<div class="member_image">
									<img src="images/member7.jpg" alt="" title="" width="170" height="170"/>
								</div>
								<div class="member_name">Դիանա Հովհաննիսյան</div>
								<div class="member_post">Գործադիր տնօրեն</div>
							</li>
							<li>
								<div class="member_image">
									<img src="images/member8.jpg" alt="" title="" width="170" height="170"/>
								</div>
								<div class="member_name">Զառա Կիրակոսյան</div>
								<div class="member_post">Հաղորդակցության բաժնի ղեկավար</div>
							</li>
						 </ul>
					 </div>
				 </div>

			 </div>

	
 		</div>
		<?php
			include 'templates/footer.php'
		?>
		<script src="js/jquery-3.6.0.min.js"></script>
	 	<script src="js/main.js"></script>
 	</body>
</html>