var $mobileSize = 960;

function isTouchDevice() {
    return 'ontouchstart' in document.documentElement;
};

function detectDevice() {
    if (navigator.userAgent.search("Safari") >= 0 && navigator.userAgent.search("Chrome") < 0) {
        $('body').addClass('ios_device');
    }
    ;
    if (isTouchDevice()) {
        $('html').addClass('touch');
    } else {
        $('html').addClass('web');
    }
}

function closeAllMenues(evt) {
    detectDevice();

    $('.drop_btn').parent().removeClass('opened');
    $('.drop_block').slideUp(300);

    if ($('.search_block').data('type') && $('.search_block').data('type') == 'close') {
        $('.search_block').removeClass('opened');
    }

    $('body').removeClass('popup_opened');
    $('.popup_block').removeClass('showed');


    if (isTouchDevice() && window.innerWidth > $mobileSize) {
        $('.header .main_menu li').removeClass('opened');
        $('.header .submenu_list').fadeOut(300);

    }
}

function ignorBodyClick(evt) {
    evt.stopPropagation();
}

function ignorMobileBodyClick(evt) {
    if (window.innerWidth < 992) {
        evt.stopPropagation();
    }
}

function dropList(dropButton, dropList, dropItem, dropElement) {
    if (dropButton.parents(dropItem).hasClass('opened')) {
        dropButton.parents(dropItem).removeClass('opened').find(dropElement).slideUp(300);
    } else {
        dropButton.parents(dropList).find('.opened').removeClass('opened');
        dropButton.parents(dropList).find(dropElement).slideUp(300);
        dropButton.parents(dropItem).addClass('opened').find(dropElement).stop(true, true).slideDown(300);
        setTimeout(function () {
            if ($(dropList).find('.opened').length > 0) {
                if (dropButton.parents(dropItem).offset().top < $(document).scrollTop()) {
                    $('body,html').animate({scrollTop: dropButton.parents(dropItem).offset().top}, 300);
                }
            }
        }, 300)
    }

};


function mobMenuTrigger(e) {
    e.preventDefault();
    if ($('body').hasClass('menu_opened')) {
        $('body').removeClass('menu_opened');
    } else {
        $('.main_menu li').removeClass('opened');
        $('.submenu_list').hide();
        $('body').addClass('menu_opened');
    }
}

function detectContentHeight() {
    var footerHeight = $('.footer').length > 0 ? $('.footer').height() : 0;
    var headerHeight = $('.header').length > 0 ? $('.header').height() : 0;
    var freeSpace = window.innerHeight - footerHeight - headerHeight;
    if (freeSpace > 0) {
        $('.content').css('min-height', freeSpace);
    } else {
        $('.content').css('min-height', 0);
    }
    ;
    $('.footer').css('opacity', 1);
}


function toggleSearch(evt) {
    if (!$('.search_block').hasClass('opened')) {
        evt.preventDefault();
        closeAllMenues(evt);
        evt.stopPropagation();
        $('.search_block').addClass('opened').find('input').focus();
    } else if (!$('.search_block input').val()) {
        $('.search_block input').focus();
        evt.preventDefault();
    } else {
        evt.stopPropagation();
    }
}

function focusEmptySearch(evt) {
    if (!$('.search_block input').val()) {
        evt.preventDefault();
        $('.search_block input').focus();
    }
}

function checkFields() {
    $('form input, form textarea').change(function () {

        if ($(this).val().length > 0) {
            $(this).parent().addClass('filled').find('.individual_hint').show();
            $(this).parents('.field_block').find('.individual_hint').show();
            $(this).parents('.field_block').find('.standard_hint').hide();
        } else {
            $(this).parent().removeClass('filled').find('.individual_hint').hide();
            $(this).parents('.field_block').find('.individual_hint').hide();
            $(this).parents('.field_block').find('.standard_hint').show();
        }

        if ($('.confirm_field').length > 0) {
            $('.confirm_field').on('keyup change', function () {
                if ($(this).val() == $(this).parents('form').find('.password_field').val()) {
                    $(this).parent().removeClass('has-error');
                    passwordConfirm = true;
                }
            })
        }
    });
}

function checkPassConfirm() {
    var passValue = $('.confirm_field').parents('form').find('.password_field').val();
    var passConfirm = $('.confirm_field').val();
    if (passValue && passValue != passConfirm && $('.pass_fields').css('display') != "none") {
        $('.confirm_field').parent().addClass('has-error');
        passwordConfirm = null;
    } else {
        passwordConfirm = true;
    }
}

function checkForm(e) {
    var $button = $(this);
    if ($button.parents('form').find('.confirm_field').length > 0) {
        checkPassConfirm();

    } else {
        passwordConfirm = true;
    }
    $.validate({
        scrollToTopOnError: false,
        onError: function () {
            if ($button.parents('form').hasClass('login_form') || $button.parents('form').hasClass('register_form')) {

                $('.has-error').each(function () {

                    var errorInputType = $(this).find('input').attr('type');
                    $('input[type="' + errorInputType + '"]').parents('.general_field').addClass('has-error');
                });

            }
            ;
        },
        onSuccess: function () {
            if (!passwordConfirm) {
                return false;
            }
        }

    });

    setTimeout(function () {
        if ($('.has-error').length > 0) {
            $('body, html').animate({scrollTop: $('.has-error').eq(0).offset().top - $('.header').height()}, 1000);
        }
    }, 100)

    // setTimeout(function () {
    //     if ($button.hasClass('checkout_submit') && $('.has-error').length > 0) {
    //         $('body, html').animate({scrollTop: $('.has-error').eq(0).offset().top - $('.header').height()}, 1000);
    //     }
    // }, 100)


};

function openLanguages(evt) {
    evt.preventDefault();
    if (!$('.language_block').hasClass('opened')) {
        closeAllMenues(evt);
        evt.stopPropagation();
        $('.language_block').addClass('opened');
        $('.language_list').stop(true, true).slideDown();
    }
    ;
}

function dropToggle(evt) {
    evt.preventDefault();
    if (!$(this).parent().hasClass('opened')) {
        closeAllMenues(evt);
        evt.stopPropagation();
        $(this).parent().addClass('opened').find('.drop_block').stop(true, true).slideDown(300);
    }
};

function openSubWithClick(evt) {
    evt.preventDefault();
    if (isTouchDevice() && window.innerWidth > $mobileSize) {
        if (!$(this).parents('li').hasClass('opened')) {
            closeAllMenues(evt);
            evt.stopPropagation();
            $(this).parents('li').addClass('opened').find('.submenu_list').stop(true, true).slideDown(300);
        }
    } else if (window.innerWidth < $mobileSize) {
        if ($(this).parents('li').hasClass('opened')) {
            $(this).parents('li').removeClass('opened').find('.submenu_list').slideUp(300);
        } else {
            $('.header .main_menu > li.opened').removeClass('opened');
            $('.header .main_menu .submenu_list').slideUp(300);
            $(this).parents('li').addClass('opened').find('.submenu_list').stop(true, true).slideDown(300);
        }
    }
}

var delayTime = null;

function openSubWithHover() {
    if (!isTouchDevice() && window.innerWidth > $mobileSize) {
        if (delayTime) {
            clearTimeout(delayTime);
        }
        ;
        var $item = $(this).parents('li');
        $item.addClass('hovered');
        delayTime = setTimeout(function () {
            if ($item.hasClass('hovered')) {
                $item.addClass('opened').find('.submenu_list').stop(true, true).slideDown(300);
            }
        }, 300)
    }
    ;
}

function mouseLeaveItem() {
    $(this).parents('li').removeClass('hovered');
}

function closeSubWithHover() {
    if (!isTouchDevice()) {
        $(this).removeClass('opened').find('.submenu_list').fadeOut(300);
    }
}

function comboHover($link, $block) {
    $link.hover(function () {
        $(this).parents($block).addClass('hovered');
    }, function () {
        $(this).parents($block).removeClass('hovered');
    })
}

function tabSwitch(e) {
    e.preventDefault();
    if (!$(this).hasClass('selected')) {
        $(this).parents('.tab_buttons').find('a').removeClass('selected');
        $(this).parents('.tab_section').find('.tab_block').removeClass('selected');
        $(this).addClass('selected');
        $('.tab_block.' + $(this).data('tab')).addClass('selected');
    }
}

function detectCallPosibillity() {
    if (/Android|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(navigator.userAgent)) {
        $('.phone_link').addClass('clickable');
    }
    $('.phone_link').click(function (e) {
        if (!$(this).hasClass('clickable')) {
            e.preventDefault();
        }
    })
}

function accordionToggle(_button, _block, _list) {
    if (_button.parent(_block).hasClass('opened')) {
        _button.parent(_block).removeClass('opened');
        _button.parent(_block).find(_list).slideUp(300);
    } else {
        $(_block).removeClass('opened');
        $(_list).slideUp(300);
        _button.parent(_block).addClass('opened').find(_list).stop(true, true).slideDown(300);
    }
}

function goToTarget() {
    var endPoint = $(this).data('endpoint');
    $('html,body').animate({scrollTop: $('[data-target="' + endPoint + '"]').offset().top - $('.header_inner').height()}, 500);
    if ($('[data-target="' + endPoint + '"]').parent().hasClass('tab_buttons')) {
        $('[data-target="' + endPoint + '"]').trigger('click');
    }
}

function openPopup(evt) {
    evt.preventDefault();
    $('body').addClass('popup_opened');
    var popupName = '.' + $(this).data('popup');
    $(popupName).addClass('showed');

}

function closePopup(evt) {
    $('body').removeClass('popup_opened');
    $('.popup_block').removeClass('showed');
    closeAllMenues(evt);
}

function changeCount(countBlock, decreaseBtn, increaseBtn, countInput) {

    $(countInput).each(function () {
        var maxValue = $(this).data('max') ? $(this).data('max') : Math.pow(10, $(this).attr('maxlength')) - 1;
        if ($(this).val() == 1) {
            $(this).parents(countBlock).find(decreaseBtn).addClass('inactive');
        } else if ($(this).val() == maxValue) {
            $(this).parents(countBlock).find(increaseBtn).addClass('inactive');
        }
    });

    $(document).on('change', countInput, function () {
        var thisDecrease = $(this).parents(countBlock).find(decreaseBtn);
        var thisIncrease = $(this).parents(countBlock).find(increaseBtn);
        var maxValue = $(this).data('max') ? $(this).data('max') : Math.pow(10, $(this).attr('maxlength')) - 1;
        if ($(this).val() <= 1) {
            $(this).val(1);
            thisDecrease.addClass('inactive');
            thisIncrease.removeClass('inactive');
        } else if ($(this).val() >= maxValue) {
            $(this).val(maxValue);
            thisIncrease.addClass('inactive');
            thisDecrease.removeClass('inactive');
        } else {
            thisIncrease.removeClass('inactive');
            thisDecrease.removeClass('inactive');
        }
    })

    $(document).on('click', decreaseBtn, function () {
        var thisInput = $(this).parent().find('input');
        var thisIncrease = $(this).parent().find(increaseBtn);
        var _value = thisInput.val();
        thisIncrease.removeClass('inactive');
        if (_value > 1) {
            _value--;
            thisInput.val(_value);
        }
        if (_value == 1) {
            $(this).addClass('inactive');
        }
    });

    $(document).on('click', increaseBtn, function () {
        var thisInput = $(this).parent().find('input');
        var thisDecrease = $(this).parent().find(decreaseBtn);
        var _value = thisInput.val();
        var maxValue = thisInput.data('max') ? thisInput.data('max') : Math.pow(10, thisInput.attr('maxlength')) - 1;
        thisDecrease.removeClass('inactive');
        if (_value < maxValue) {
            _value++;
            thisInput.val(_value);
        }
        if (_value == maxValue) {
            $(this).addClass('inactive');
        }
    });
}

function initDatePicker($dateInput) {
    var daysList = {
        "en": ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
        "am": ["Կի", "Եկ", "Եք", "Չո", "Հի", "Ու", "Շա"],
        "ru": ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"]
    };

    var monthsList = {
        "en": ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
        "am": ["Հունվար", "Փետրվար", "Մարտ", "Ապրիլ", "Մայիս", "Հունիս", "Հուլիս", "Օգոստոս", "Սեպտեմբեր", "Հոկտեմբեր", "Նոյեմբեր", "Դեկտեմբեր"],
        "ru": ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"]
    };

    var rangesList = {
        "en": ["Today", "Yesterday", "Last 7 Days", "Last 30 Days", "This Month", "Last Month"],
        "am": ["Այսօր", "Երեկ", "Վերջին 7 օր", "Վերջին 30 օր", "Այս ամիս", "Վերջին ամիս"],
        "ru": ["Сегодня", "Вчера", "Последние 7 дней", "Последние 30 дней", "Этот месяц", "Последний месяц"]
    }

    var cancelBtnLabel = {
        "en": "From Beginning",
        "ru": "С начала",
        "am": "Սկզբից"
    };

    $dateInput.each(function () {
        var $parrent = $(this).parent();
        var $dateInput = $(this);
        var $dateFormat = $dateInput.data('format') ? $dateInput.data('format') : 'DDDD.MM.YY';
        var $dateLg = $dateInput.data('lg') ? $dateInput.data('lg') : 'en';
        var $autoUpdate = $dateInput.data('update') ? true : false;
        var $dateRanges = $dateInput.data('ranges') ? {
            [rangesList[$dateLg][0]]: [moment(), moment()],
            [rangesList[$dateLg][1]]: [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            [rangesList[$dateLg][2]]: [moment().subtract(6, 'days'), moment()],
            [rangesList[$dateLg][3]]: [moment().subtract(29, 'days'), moment()],
            [rangesList[$dateLg][4]]: [moment().startOf('month'), moment().endOf('month')],
            [rangesList[$dateLg][5]]: [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        } : null;


        $dateInput.daterangepicker({
            opens: 'left',
            autoUpdateInput: $autoUpdate,
            maxDate: new Date(),
            parentEl: $parrent,
            singleDatePicker: true,
            alwaysShowCalendars: true,
            showCustomRangeLabel: false,
            showDropdowns: true,
            autoApply: true,
            locale: {
                format: $dateFormat,
                daysOfWeek: daysList[$dateLg],
                monthNames: monthsList[$dateLg],
                cancelLabel: cancelBtnLabel[$dateLg],
                firstDay: 1
            },
            ranges: $dateRanges,
            onSelect: function (arg) {
                $dateInput.val($dateInput.attr("placeholder") + arg);
            },

        }, function (chosen_date) {
            $dateInput.val(chosen_date.format($dateFormat));
        });
    });


    $dateInput.on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
    });
}

function initVideoSlider($slider) {
    var slideBlock = $slider.find('.slide_block');
    slideBlock.eq(0).addClass('active').find('video')[0].play();

    function changeSlide() {
        var activeSlide = $slider.find('.active');
        var nextSlide = activeSlide.next().length > 0 ? activeSlide.next() : slideBlock.eq(0);
        var currentVideo = activeSlide.find('video')[0];
        currentVideo.onended = function () {
            activeSlide.removeClass('active');
            nextSlide.addClass('active').find('video')[0].play();
            changeSlide();
        }
    }

    changeSlide();
}

function inityMap($map) {
    var myMap = null;
    var initBlock = $map.attr('id').replace('#', '');
    var _coords = [$map.data('coords').split(',')[0] * 1, $map.data('coords').split(',')[1] * 1];
    var markerImage = $map.data('markerimage') ? $map.data('markerimage') : null;
    var markerSize = $map.data('markersize') ? [$map.data('markersize').split(',')[0] * 1, $map.data('markersize').split(',')[1] * 1] : [60, 60];
    var markerOffset = $map.data('markersize') ? [-$map.data('markersize').split(',')[0] * 0.5, -$map.data('markersize').split(',')[1] * 0.5] : [-30, -30];
    var defMarker = $map.data('defmarker') ? $map.data('defmarker') : '';
    var defMarkerColor = $map.data('defmarkercolor') ? $map.data('defmarkercolor') : "yellow";
    var mapZoom = $map.data('zoom') ? $map.data('zoom') : 16;
    var $marker = markerImage ? {
        iconLayout: 'default#image',
        iconImageHref: markerImage,
        iconImageSize: markerSize,
        iconImageOffset: markerOffset
    } : {
        iconColor: defMarkerColor,
        preset: defMarker,
    };

    ymaps.ready(function () {
        myMap = new ymaps.Map(initBlock, {
            center: _coords,
            zoom: mapZoom,
            controls: []
        });

        if (isTouchDevice()) {
            myMap.behaviors.disable('drag');
        } else {
            myMap.behaviors.disable('scrollZoom');
        }

        var placemark = new ymaps.Placemark(myMap.getCenter(), {}, $marker);
        myMap.geoObjects.add(placemark)
    });
}


const phoneMaskSelector = '.phone_number';
const phoneMaskInputs = document.querySelectorAll(phoneMaskSelector);

const masksOptions = {
    phone: {
        mask: '+{374} (00) 00-00-00'
    }
};

for (const item of phoneMaskInputs) {
    new IMask(item, masksOptions.phone);
}

function checkFieldsStatus() {
    $('input, textarea').each(function () {
        if ($(this).val().length > 0) {
            $(this).addClass('filled');
            if ($(this).val() != '') {
                $(this).parent().addClass('filled');
            }
        }
    });
    setTimeout(function () {
        $('.placeholder').css('opacity', 1);
    }, 300)
}

$('.field_name_switch').click(function() {
    $('.regular_switch_custom').toggleClass( "regular_switch_block" );
});


$(document).ready(function () {
    //detect device type
    detectDevice();
    detectCallPosibillity();
    checkFieldsStatus();

    //close dropdowns with outside click
    $('body').click(closeAllMenues);

    //opening/closing mobile menu
    $('.menu_btn').click(mobMenuTrigger);
    $('.header .submenu_btn').hover(openSubWithHover, mouseLeaveItem);
    $('.header .main_menu > li').hover(function () {
    }, closeSubWithHover);
    $('.header .submenu_btn').click(openSubWithClick);

    // form front validation
    if ($('.validate_btn').length > 0) {
        checkFields();
        $('.validate_btn').click(checkForm);
    }
    ;

    //drop element open close
    $('.drop_btn').click(dropToggle);

    //hidden search open/close
    $('.search_block button[type="submit"]').click(function (evt) {
        if ($('.search_block').data('type') && $('.search_block').data('type') == 'close') {
            toggleSearch(evt);
        } else {
            focusEmptySearch(evt);
        }
    });

    $('.search_block input').click(function (evt) {
        if ($('.search_block').data('type') && $('.search_block').data('type') == 'close') {
            ignorBodyClick(evt);
        }
    });

    if ($('.header').length > 0) {
        $(window).scroll(function () {
            if ($(document).scrollTop() > 100) {
                $('.header').addClass('compact');
            } else {
                $('.header').removeClass('compact');
            }
        }).trigger('scroll');
    }

    if ($('.main_slider').length > 0) {
        $(window).resize(function () {
            $('.main_slider').innerHeight(window.innerHeight);
        }).trigger('resize');
        initVideoSlider($('.main_slider'));
    }
    ;

    if ($('.news_slider').length > 0) {
        $('.news_slider').slick({
            slidesToShow: 3,
            responsive: [
                {
                    breakpoint: 960,
                    settings: {
                        slidesToShow: 2,
                    }
                },
                {
                    breakpoint: 576,
                    settings: {
                        slidesToShow: 1,
                    }
                }
            ]
        })
    }
    ;

    if ($('.images_slider').length > 0) {
        $('.images_slider .slider_list').slick({
            slidesToShow: 4,
            responsive: [
                {
                    breakpoint: 960,
                    settings: {
                        slidesToShow: 3,
                    }
                },
                {
                    breakpoint: 640,
                    settings: {
                        slidesToShow: 2,
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                    }
                }
            ]
        })
    }
    ;

    if ($('.gallery_list').length > 0) {
        $('.gallery_list').masonry({
            itemSelector: '.gallery_list li',
            columnWidth: '.gallery_list li',
            percentPosition: true,
            transitionDuration: 0
        });
    }
    ;

    if ($('.news_block').length > 0) {
        comboHover($('.news_block a'), '.news_block')
    }
    ;

    if ($('.map_block').length > 0) {
        setTimeout(function () {
            inityMap($('#map'));
        }, 100)

    }

    if ($(".faq_list").length > 0) {
        $('.question_block').click(function () {
            if ($(this).parents('li').hasClass('opened') && window.innerWidth < 768) {
                $(this).parents('li').removeClass('opened').find('answer_block').slideUp(300);
            } else if (!$(this).parents('li').hasClass('opened')) {
                $('.faq_list li.opened').removeClass('opened');
                $(this).parents('li').addClass('opened');
                if (window.innerWidth < 768) {
                    $('.answer_block').slideUp(300);
                    $(this).parents('li').find('.answer_block').stop(true, true).slideDown(300);
                    setTimeout(function () {
                        if ($(document).scrollTop() + 50 >= $('.faq_list li.opened').offset().top) {
                            $('body,html').animate({scrollTop: $('.faq_list li.opened').offset().top - 50}, 500);
                        }
                    }, 300)
                } else {
                    $('.answer_block').fadeOut(300);
                    $(this).parents('li').find('.answer_block').stop(true, true).fadeIn(300);
                    setTimeout(function () {
                        if ($(document).scrollTop() + 50 >= $('.faq_list').offset().top) {
                            $('body,html').animate({scrollTop: $('.faq_list').offset().top - 50}, 500);
                        }
                    }, 300)
                }
            }

        });
        setTimeout(function () {
            $('.faq_list > li:first-child .question_block').trigger('click');
        }, 100)

    }
    ;

    if ($('.video_btn').length > 0) {
        $('.video_btn').click(function () {
            if ($(this).parent().hasClass('playing')) {
                $(this).parent().removeClass('playing').find('video')[0].pause();
            } else {
                $(this).parent().addClass('playing').find('video')[0].play();
                $(this).parent().find('video').bind('ended', function () {
                    console.log('sdfdsfdsfsdf');
                    $(this).parent().removeClass('playing');
                })
            }
        });
    }

    if ($('.date_input').length > 0) {
        initDatePicker($('.date_input'));
    }

    if ($('select').length > 0) {
        var selectParent = $('select').parents('.field_block');
        var $placeholder = $('select').data('placeholder');
        $('select').select2({
            minimumResultsForSearch: Infinity,
            width: '100%',
            placeholder: $placeholder,
            //dropdownParent: selectParent
        });
    }
    ;

    $('.popup_close').click(closePopup);

    $('.switch_btn').click(function () {
        $(this).parents('.switch_content').addClass('opened').find('.switch_block.active').removeClass('active');
        $(this).parents('.switch_content').find('.switch_block.' + $(this).data('type')).addClass('active');
        if ($('.switch_content').data('hash')) {
            window.location.hash = $(this).data('type');
        }
    });

    $('.switch_block .back_btn').click(function () {
        $(this).parents('.switch_content').removeClass('opened').find('.switch_block.active').removeClass('active');
        if (window.location.hash) {
            history.pushState("", document.title, window.location.pathname + window.location.search);
        }
    });

    if ($('.switch_content').length > 0) {
        if (window.location.hash && $('.switch_btn').length > 0) {
            $active = window.location.hash.toString().replace('#', '');
            $('.switch_btn[data-type="' + $active + '"]').trigger('click');
        }
        setTimeout(function () {
            $('.switch_content').animate({opacity: 1}, 300)
        }, 100);

        if ($('.ammounts_list').length > 0) {
            $('.ammounts_list>li>label>input:checked').each(function () {
                $(this).parents('.switch_block').find('.donate_size input').val($(this).data('value'));
                $(this).parents('.switch_block').find('.showed_size').text($(this).data('value'));
            })
        }
        ;

        $('.ammounts_list>li>label>input').on('change', function () {
            $('.custom_size label>input').val('');
            $('.other_block').removeClass('other_block_active');
            $(this).parents('.switch_block').find('.donate_size input').val($(this).data('value'));
            $(this).parents('.switch_block').find('.showed_size').text($(this).data('value'));
        });

        if ($('.currency_list').length > 0) {
            $('.currency_list input:checked').each(function () {
                $(this).parents('.switch_block').find('.currency_size input').val($(this).data('value'));
                $(this).parents('.switch_block').find('.showed_currency').text($(this).data('value'));
                $(this).parents('.switch_block').find('.ammounts_list.' + $(this).data('type')).addClass('showed');
                $(this).parents('.switch_block').find('.donate_size input').val($('.ammounts_list.showed>li>label>input:checked').data('value'));
                $(this).parents('.switch_block').find('.showed_size').text($('.ammounts_list.showed>li>label>input:checked').data('value'));
                $(this).parents('.switch_block').find('.donate_size .error_hint .min_size').text($(this).data('value') + ' ' + $(this).data('min'));
                $(this).parents('.switch_block').find('.donate_size .error_hint .max_size').text($(this).data('value') + ' ' + $(this).data('max'));
                $(this).parents('.switch_block').find('.donate_size input').attr('data-validation-allowing', 'range[' + $(this).data('min') + ';' + $(this).data('max') + ']');
            })
        }
        ;

        $('.currency_list input').on('change', function () {
            $('.custom_size label>input').val('');
            $('.other_block').removeClass('other_block_active');
            $(this).parents('.switch_block').find('.ammounts_list').removeClass('showed');
            $(this).parents('.switch_block').find('.ammounts_list.' + $(this).data('type')).addClass('showed');
            if ($(this).parents('.switch_block').find('.ammounts_list.showed>li>label>input:checked').length < 1) {
                $(this).parents('.switch_block').find('.ammounts_list.showed>li>label>input[data-favorite]').prop('checked', 'checked');
            }
            $(this).parents('.switch_block').find('.donate_currency input').val($(this).data('value'));
            $(this).parents('.switch_block').find('.showed_currency').text($(this).data('value'));
            $(this).parents('.switch_block').find('.donate_size input').val($('.ammounts_list.showed>li>label>input:checked').data('value'));
            $(this).parents('.switch_block').find('.showed_size').text($('.ammounts_list.showed>li>label>input:checked').data('value'));
            $(this).parents('.switch_block').find('.donate_size .error_hint .min_size').text($(this).data('value') + ' ' + $(this).data('min'));
            $(this).parents('.switch_block').find('.donate_size .error_hint .max_size').text($(this).data('value') + ' ' + $(this).data('max'));
            $(this).parents('.switch_block').find('.donate_size input').attr('data-validation-allowing', 'range[' + $(this).data('min') + ';' + $(this).data('max') + ']');
        });

        $('.custom_size label>input').on('keyup', function () {
            let other_blocks = $(this).val();
            if (other_blocks != '') {
                $('.other_block').addClass('other_block_active');
            } else {
                $('.other_block').removeClass('other_block_active');
            }
            if ($(this).val()) {
                if ($(this).parents('.switch_block').hasClass('by_paypal')) {
                    $(this).parents('.switch_block').find('.ammounts_list>li>label>input:checked').prop('checked', false);
                } else {
                    $(this).parents('.switch_block').find('.ammounts_list.showed>li>label>input:checked').prop('checked', false);
                }
                $(this).parents('.switch_block').find('.donate_size input').val($(this).val());
                $(this).parents('.switch_block').find('.showed_size').text($(this).val());
            }
        });
        $('.custom_size label>input').on('change', function () {
            $('.other_block').addClass('other_block_active');
            if ($(this).parents('.switch_block').hasClass('by_paypal')) {
                $(this).parents('.switch_block').find('.ammounts_list>li>label>input:checked').prop('checked', false);
            } else {
                $(this).parents('.switch_block').find('.ammounts_list.showed>li>label>input:checked').prop('checked', false);

            }
            $(this).parents('.switch_block').find('.donate_size input').val($(this).val());
            $(this).parents('.switch_block').find('.showed_size').text($(this).val());
        })

    }
    ;

    $('.popup_btn').click(openPopup);

    $('.copy_btn').on('click', function (e) {
        e.preventDefault();
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val($(this).parents('tr').find('.account_number').html()).select();
        document.execCommand("copy");
        $temp.remove();
    });

});

$(window).on('load', function () {
    $(window).resize(function () {
        //detect content min height and show footer
        detectContentHeight();

    }).trigger('resize');
})