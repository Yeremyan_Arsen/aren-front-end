  <!DOCTYPE HTML>
<html lang="am">
 	<head>
  		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0, viewport-fit=cover"/>
		<meta name="format-detection" content="telephone=no"/>
		<meta name="description" content="Aren Mehrabyan foundation website">
  		<title>ԱՐԵՆ ՄԵՀՐԱԲՅԱՆ ՀԻՄՆԱԴՐԱՄ</title>
		<link rel="stylesheet" href="css/main.css">
		<link rel="stylesheet" href="css/jquery.fancybox.css">
		<link rel="stylesheet" href="css/about.css">
		<?php
			include 'templates/favicons.php'
		?>
 	</head>
 	<body>
 		<?php
			include 'templates/header.php'
		?>
 		<div class="content">
			<div class="page_head_block">
				<div class="head_inner">
					<div class="page_container">
						<div class="info_block">
							<h1 class="page_title">Մեր Շրջանավարտները</h1>
						</div>
						<div class="image_block">
							<img src="images/graduates_top_image.jpg" alt="" title="" width="1160" height="700"/>
						</div>
					</div>
				</div>
			</div>

			<div class="stats_section">
				<div class="page_container">
					<ul class="stats_list">
						<li>
							<div class="count_block">120</div>
							<div class="type_block">Շրջանավարտ տարբեր դասընթացներից</div>
						</li>
						<li>
							<div class="count_block">10</div>
							<div class="type_block">Աշխատանքի են անցել PicsArt-ում</div>
						</li>
						<li>
							<div class="count_block">65</div>
							<div class="type_block">Աշխատանքի են անցել այլ կազմակերպություններում</div>
						</li>
					</ul>
					<div class="info_block">Հիմնադրամի կազմակերպած դասընթացների ավարտին ուսանողները ստանում են մասնագիտական բազային գիտելիքներ և հնարավորություն ուժերը փորձելու տարբեր կազմակերպություններում։
						<br/>Միաժամանակ հիմնադրամը փորձում է լուծել նաև աշխատանք գտնելու խնդիրը։ Հայաստանում գործող մի շարք կազմակերպություններ հիմնադրամի 15 ուսանողների արդեն իսկ ընդունել են աշխատանքի։
					</div>
				</div>
			</div>

			<div class="gallery_section">
				<div class="page_container">
					<h2 class="page_title">Պատկերասրահ</h2>
					<ul class="gallery_list">
						<li>
							<a href="images/gallery_image1.jpg" data-fancybox="gallery">
								<img src="images/gallery_image1.jpg" alt="" title="" width="360" height="477"/>
								gallery image name
							</a>
						</li>
						<li>
							<a href="images/gallery_image2.jpg" data-fancybox="gallery">
								<img src="images/gallery_image2.jpg" alt="" title="" width="360" height="453"/>
								gallery image name
							</a>
						</li>
						<li>
							<a href="images/gallery_image3.jpg" data-fancybox="gallery">
								<img src="images/gallery_image3.jpg" alt="" title="" width="360" height="258"/>
								gallery image name
							</a>
						</li>
						<li>
							<a href="images/gallery_image6.jpg" data-fancybox="gallery">
								<img src="images/gallery_image6.jpg" alt="" title="" width="360" height="482"/>
								gallery image name
							</a>
						</li>
						<li>
							<a href="images/gallery_image5.jpg" data-fancybox="gallery">
								<img src="images/gallery_image5.jpg" alt="" title="" width="360" height="287"/>
								gallery image name
							</a>
						</li>
						<li>
							<a href="images/gallery_image4.jpg" data-fancybox="gallery">
								<img src="images/gallery_image4.jpg" alt="" title="" width="360" height="263"/>
								gallery image name
							</a>
						</li>
					</ul>
				</div>
			</div>
 		</div>
		<?php
			include 'templates/footer.php'
		?>
		<script src="js/jquery-3.6.0.min.js"></script>
		<script src="js/masonry.pkgd.js"></script>
		<script src="js/jquery.fancybox.js"></script>
	 	<script src="js/main.js"></script>
 	</body>
</html>