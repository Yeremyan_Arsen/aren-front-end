  <!DOCTYPE HTML>
<html lang="am">
 	<head>
  		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0, viewport-fit=cover"/>
		<meta name="format-detection" content="telephone=no"/>
		<meta name="description" content="Aren Mehrabyan foundation website">
  		<title>ԱՐԵՆ ՄԵՀՐԱԲՅԱՆ ՀԻՄՆԱԴՐԱՄ</title>
		<link rel="stylesheet" href="css/main.css">
		<link rel="stylesheet" href="css/programs.css">
		<?php
			include 'templates/favicons.php'
		?>
 	</head>
 	<body>
 		<?php
			include 'templates/header.php'
		?>
 		<div class="content">
			<div class="page_head_block">
				<div class="head_inner">
					<div class="page_container">
						<div class="info_block">
							<h1 class="page_title">Աերոտիեզերական հայկական ծրագիր</h1>
						</div>
						<div class="image_block">
							<img src="images/aerospace_top_image.jpg" alt="" title="" width="1160" height="700"/>
						</div>
					</div>
				</div>
			</div>

			<div class="program_purpose">
				<div class="page_container">
					<h2 class="page_title">Ծրագրի նպատակը</h2>
					<div class="info_block">Արենի երազանքն էր Հայաստանում հիմնել և իրականացնել աերոտիեզերական շարժիչների և թռչող սարքերի հետազոտման, հետագայում արդեն արտադրական կենտրոն: Հիմնվելով նրա երազանքների վրա՝ նախատեսում ենք  պատրաստել համապատասխան մասնագետներ և հիմնել լաբորատորիաներ, որոնք կպատրաստեն աերոտիեզերական շարժիչներ և թռչող սարքեր: 
					<br/>
					<br/>Այս նպատակով Արմավիրի մարզում, Աշտարակից Էջմիածին մայրուղու եզրին ձեռք է բերվել 80 հա հողատարածք, ուր կկառուցվի աեորոտիեզերական շարժիչների և թռչող սարքերի գործարան: Այստեղ կաշխատեն պատերազմում հաշմանդամություն ստացած վիրավոր, մասնակից տղաները, զոհվածների ընտանիքների անդամները, ովքեր արդեն իսկ մասնագիտություն են ձեռք բերել «Արեն Մեհրաբյան» բարեգործական հիմնադրամում: Այս գործարանի կառուցումը կնպաստի նաև գյուղական համայնքների ներգրավմանը և տնտեսականը բարելավմանը:
					</div>
				</div>
			</div>

			<div class="info_section">
				<div class="page_container">
					<h2 class="page_title">Ծրագրի մանրամասները</h2>
					<div class="info_block">
						<div class="inner_block">
							<div class="text_block">
								Հողատարածք
								<div class="include_info">Արմավիրի մարզում, Աշտարակից Էջմիածին մայրուղու եզրին ձեռք է բերվել 80 հա</div>
							</div>
						</div>
						<div class="inner_block">
							<div class="text_block">
								Ծրագրի ժամկետները
								<div class="include_info">Ծրագրի իրականացումը նախատեսվում է 2021-2025թթ ընթացքում</div>
							</div>
						</div>
						<div class="inner_block">
							<div class="text_block">
								Ծրագրի բյուջեն
								<div class="include_info">Ընդհանուր բյուջեն կզմում է $ 3,526,230․00</div>
							</div>
						</div>
						<div class="inner_block">
							<div class="text_block">
								Աշխատատեղերի քանակը
								<div class="include_info">Ընդհանուր աշխատանքով կապահովեն մոտ 120 հոգի</div>
							</div>
						</div>
					</div>
					<div class="image_block">
						<video preload="auto">
							<source type="video/mp4" src="video/inner_video.mp4#t=1"/>
						</video>
						<button class="video_btn" aria-label="video"></button>
					</div>
				</div>
			</div>

			<div class="additional_info">
				<div class="page_container">
					<div class="info_block">Ծրագրին մանրամասն ծանոթանալու համար կարող եք այցելել <a href="https://www.engined.am" target="_blank">engined.am</a> կայքը։</div>
				</div>
			</div>
 		</div>
		<?php
			include 'templates/footer.php'
		?>
		<script src="js/jquery-3.6.0.min.js"></script>
	 	<script src="js/main.js"></script>
 	</body>
</html>