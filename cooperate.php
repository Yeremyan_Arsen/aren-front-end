  <!DOCTYPE HTML>
<html lang="am">
 	<head>
  		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0, viewport-fit=cover"/>
		<meta name="format-detection" content="telephone=no"/>
		<meta name="description" content="Aren Mehrabyan foundation website">
  		<title>ԱՐԵՆ ՄԵՀՐԱԲՅԱՆ ՀԻՄՆԱԴՐԱՄ</title>
		<link rel="stylesheet" href="css/main.css">
		<link rel="stylesheet" href="css/cooperate.css">
		<?php
			include 'templates/favicons.php'
		?>
 	</head>
 	<body>
 		<?php
			include 'templates/header.php'
		?>
 		<div class="content">
			<div class="cooperate_top">
				<div class="image_block">
					<img src="images/cooperate_top_image.jpg" alt="" title="" width="1920" height="520"/>
				</div>
				<div class="info_block">
					<div class="page_container">
						<div class="info_inner switch_content">
							<div class="info_preview">
								<h2 class="page_title">Համագործակցել</h2>
                                <div class="cooperate_description">
                                    Ուրախ կլինենք համագործակցել Ձեզ հետ, եթե ցանկանում եք աջակցել «Արեն Մեհրաբյան» բարեգործական հիմնադրամին ու նրա ծրագրերի  իրականացմանն ու զարգացմանը:
                                </div>
								<div class="btns_block">
									<button class="primary_btn switch_btn" data-type="individual" aria-label="individual">Անհատ</button>
									<button class="primary_btn switch_btn" data-type="organization" aria-label="organization">Կազմակերպություն</button>
								</div>
							</div>
							<div class="switch_block individual">
                                <a href="" class="switch_top">
                                    <!--                            <button class="back_btn icon_left" aria-label="back"></button>-->
                                    <h2 class="page_title icon_left">Անհատ</h2>
                                </a>
								<form>
									<div class="field_block">
										<label>
											<input type="text" autocomplete="off" name="name_surname" data-validation="required"/>
                                            <span class="placeholder">Անուն ազգանուն</span>
											<span class="label">Անուն ազգանուն</span>
										</label>
										<span class="error_hint">պարտադիր դաշտ</span>
									</div>
									<div class="field_block">
										<label>
											<input type="text" autocomplete="off" name="email" data-validation="email"/>
                                            <span class="placeholder">Էլ. հասցե</span>
											<span class="label">Էլ. հասցե</span>
										</label>
										<span class="error_hint">
											<span class="standard_hint">պարտադիր դաշտ</span>
											<span class="individual_hint">սխալ էլ. հասցե</span>
										</span>
									</div>
									<div class="field_block">
										<label>
											<textarea name="message" data-validation="required"></textarea>
                                            <span class="placeholder">Ձեր նամակը</span>
											<span class="label">Փեր նամակը</span>
										</label>
										<span class="error_hint">պարտադիր դաշտ</span>
									</div>
									<div class="btn_block">
										<button type="submit" class="validate_btn primary_btn" aria-label="submit">Ուղարկել</button>
									</div>
								</form>
							</div>
							<div class="switch_block organization">
                                <a href="" class="switch_top">
                                    <!--                            <button class="back_btn icon_left" aria-label="back"></button>-->
                                    <h2 class="page_title icon_left">Կազմակերպություն</h2>
                                </a>
								<form>
									<div class="field_block">
										<label>
											<input type="text" autocomplete="off" name="name_surname" data-validation="required"/>
                                            <span class="placeholder">Անուն ազգանուն</span>
											<span class="label">Անուն ազգանուն</span>
										</label>
										<span class="error_hint">պարտադիր դաշտ</span>
									</div>
									<div class="field_block">
										<label>
											<input type="text" autocomplete="off" name="email" data-validation="email"/>
                                            <span class="placeholder">Էլ. հասցե</span>
											<span class="label">Էլ. հասցե</span>
										</label>
										<span class="error_hint">
											<span class="standard_hint">պարտադիր դաշտ</span>
											<span class="individual_hint">սխալ էլ. հասցե</span>
										</span>
									</div>
									<div class="field_block">
										<label>
											<input type="text" autocomplete="off" name="company_name" data-validation="required"/>
                                            <span class="placeholder">Կազմակերպության անուն</span>
											<span class="label">Կազմակերպության անուն</span>
										</label>
										<span class="error_hint">պարտադիր դաշտ</span>
									</div>
									<div class="field_block">
										<label>
											<span class="label">Վեբ կայք</span>
											<input type="text" autocomplete="off" name="company_website"/>
                                            <span class="placeholder">Վեբ կայք</span>
										</label>
										<span class="error_hint">պարտադիր դաշտ</span>
									</div>
									<div class="field_block">
										<label>
											<textarea name="message" data-validation="required"></textarea>
                                            <span class="placeholder">Ձեր նամակը</span>
											<span class="label">Փեր նամակը</span>
										</label>
										<span class="error_hint">պարտադիր դաշտ</span>
									</div>
									<div class="btn_block">
										<button type="submit" class="validate_btn primary_btn" aria-label="submit">Ուղարկել</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="partners_section">
				<div class="page_container">
					<h2 class="page_title">Մեզ հետ համագործակցում են</h2>
					<div class="partners_group">
						<div class="group_title">Կազմակերպություններ</div>
						<ul>
							<li>
								<a href="#" target="_blank" class="partner_block">
									<span class="image_block">
										<img src="images/partner1.jpg" alt="" title="" width="210" height="100"/>
									</span>
									<span class="name_block">Եվրոպացի և Հայ Մասնագետների Ասոցիացիա - <br/>ՀԵՄԱ ՀԿ</span>
								</a>
							</li>
							<li>
								<a href="#" target="_blank" class="partner_block">
									<span class="image_block">
										<img src="images/partner2.jpg" alt="" title="" width="160" height="120"/>
									</span>
									<span class="name_block">«Հերոսների վերականգնողական քաղաք» <br/>ՀԿ</span>
								</a>
							</li>
							<li>
								<a href="#" target="_blank" class="partner_block">
									<span class="image_block">
										<img src="images/partner3.jpg" alt="" title="" width="120" height="120"/>
									</span>
									<span class="name_block">Տեխնոլոգիական կրթության ակադեմիային (TTA)</span>
								</a>
							</li>
							<li>
								<a href="#" target="_blank" class="partner_block">
									<span class="image_block">
										<img src="images/partner4.jpg" alt="" title="" width="160" height="120"/>
									</span>
									<span class="name_block">FESTO ընկերություն</span>
								</a>
							</li>
						</ul>
					</div>
					<div class="partners_group">
						<div class="group_title">Անհատներ</div>
						<ul>
							<li>
								<div class="partner_block">
									<span class="image_block">
										<img src="images/partner5.jpg" alt="" title="" width="210" height="120"/>
									</span>
									<span class="name_block">Արմեն Բայրամյան
										<br/>ՊՆ փոխգնդապետ</span>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
 		</div>
		<?php
			include 'templates/footer.php'
		?>
		<div class="submit_popup popup_block">
			<div class="popup_inner">
				<div class="popup_container">
					<button class="icon_close popup_close" aria-label="close"></button>
					<div class="page_title">Ձեր դիմումը հաջողությամբ ուղարկված է</div>
					<span class="status_icon icon_checked"></span>
					<!-- <div class="page_title">Տեղի է ունեցել սխալ, խնդրում ենք փորձել մի փոքր ուշ</div>
					<span class="status_icon icon_close"></span> -->
				</div>
			</div>
		</div>
		<script src="js/jquery-3.6.0.min.js"></script>
		<script src="js/jquery.form-validator.js"></script>
	 	<script src="js/main.js"></script>
 	</body>
</html>