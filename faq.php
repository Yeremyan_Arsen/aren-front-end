  <!DOCTYPE HTML>
<html lang="am">
 	<head>
  		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0, viewport-fit=cover"/>
		<meta name="format-detection" content="telephone=no"/>
		<meta name="description" content="Aren Mehrabyan foundation website">
  		<title>ԱՐԵՆ ՄԵՀՐԱԲՅԱՆ ՀԻՄՆԱԴՐԱՄ</title>
		<link rel="stylesheet" href="css/main.css">
		<link rel="stylesheet" href="css/faq.css">
		<?php
			include 'templates/favicons.php'
		?>
 	</head>
 	<body>
 		<?php
			include 'templates/header.php'
		?>
 		<div class="content">
			<div class="page_title_block">
				<div class="page_container">
					<h1 class="page_title">Հաճախ տրվող հարցեր</h1>
				</div>
			</div>
			<div class="faq_section">
				<div class="page_container">
					<ul class="faq_list">
						<li>
							<button class="question_block" aria-label="question">Ովքե՞ր կարող են հանդիսանալ հիմնադրամի շահառու:</button>
							<div class="answer_block">
								<div class="answer_inner">Նախընտրում ենք համագործակցել այլ բարեգործական կազմակերպությունների, հիմնադրամների, ոչ առևտրային կազմակերպությունների հետ, որոնց ծրագրերը կհամադրվեն մեր չափորոշիչներին՝ խնայելով ֆինանսական, ժամանակային ռեսուրսները:
									<br/>
									<br/>Եթե ցանկանում եք նվիրաբերել նշված առավելագույն գումարից ավել, խնդրում ենք ուղիղ փոխանցում կատարել հետևյալ բանկային հաշվեհամարներին: 
									<br/>
									<br/>Հետևեք նշված հղմանը՝
									<br/><a href="donate.php" class="primary_btn">Նվիրաբերել</a>
								</div>
							</div>
						</li>
						<li>
							<button class="question_block" aria-label="question">Որպես շրջանավարտ՝ ի՞նչ գործունեությամբ կարող եմ
								զբաղվել հետագայում:</button>
							<div class="answer_block">
								<div class="answer_inner">
									Ուշադիր կարդալով ընդունելության հայտաձևը՝ չափորոշիչներին համապատասխանող յուրաքանչյուր անձ կարող է լրացնել այն ու ընդգրկվել մեր կրթական համակարգում: 
									<br/>
									<br/>Բոլոր ծրագրերն անվճար են` բարեգործական հիմունքներով:
									<br/>
									<br/><a href="apply.php" class="primary_btn">Դիմել դասընտացին</a>
								</div>
							</div>
						</li>
						<li>
							<button class="question_block" aria-label="question">Ինչպե՞ս գրանցվել դասընթացներին:</button>
							<div class="answer_block">
								<div class="answer_inner">Եթե ցանկանում եք Ձեր ներդրումն ունենալ Հայաստանում գիտատեխնիկական առաջընթացին, ապա կարող եք համագործակցել նաև այլ կերպ՝ գրելով մեր էլեկտրոնային փոստին:
									<br/>
									<br/><a href="cooperate.php" class="primary_btn">Համագործակցել</a>
								</div>
							</div>
						</li>
						<li>
							<button class="question_block" aria-label="question">Կա՞ն արդյոք վճարովի դասընթացներ:</button>
							<div class="answer_block">
								<div class="answer_inner">
									<ul>
										<li>Պատերազմի մասնակից  զինծառայողներ,</li>
										<li>Պատերազմի ընթացքում վիրավորում ստացած և / կամ հաշմանդամություն ունեցող անձինք, ինչպես նաև զոհված զինծառայողների ընտանիքների անդամներ,</li>
										<li>Գիտական, տեխնիկական մասնագիտություններով հետաքրքրված անձինք</li>
									</ul>
								</div>
							</div>
						</li>
						<li>
							<button class="question_block" aria-label="question">Ինչպե՞ս վարել դասախոսություններ:</button>
							<div class="answer_block">
								<div class="answer_inner"></div>
							</div>
						</li>
						<li>
							<button class="question_block" aria-label="question">Ինչպե՞ս կարող է անհատը կամ կազմակերպությունը համագործակցել հիմնադրամի հետ:</button>
							<div class="answer_block">
								<div class="answer_inner"></div>
							</div>
						</li>
						<li>
							<button class="question_block" aria-label="question">Ի՞նչ այլ ֆինանսավորողների հետ եք սերտորեն համագործակցում:</button>
							<div class="answer_block">
								<div class="answer_inner"></div>
							</div>
						</li>
					</ul>
				</div>
			</div>
 		</div>
		<?php
			include 'templates/footer.php'
		?>
		<script src="js/jquery-3.6.0.min.js"></script>
	 	<script src="js/main.js"></script>
 	</body>
</html>