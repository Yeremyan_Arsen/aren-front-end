  <!DOCTYPE HTML>
<html lang="am">
 	<head>
  		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0, viewport-fit=cover"/>
		<meta name="format-detection" content="telephone=no"/>
		<meta name="description" content="Aren Mehrabyan foundation website">
  		<title>ԱՐԵՆ ՄԵՀՐԱԲՅԱՆ ՀԻՄՆԱԴՐԱՄ</title>
		<link rel="stylesheet" href="css/main.css">
		<link rel="stylesheet" href="css/apply.css">
		<?php
			include 'templates/favicons.php'
		?>
 	</head>
 	<body>
 		<?php
			include 'templates/header.php'
		?>
 		<div class="content">
			<div class="page_title_block">
				<div class="page_container">
					<h1 class="page_title">Դիմել դասընթացին</h1>
				</div>
			</div>
			<div class="apply_content">
				<div class="page_container">
					<ul class="apply_steps">
						<li class="current_step"><a class="step_block" href="apply.php">Քայլ 1</a></li>
						<li><a class="step_block inactive" href="apply2.php">Քայլ 2</a></li>
						<li><a class="step_block inactive" href="apply3.php">Քայլ 3</a></li>
						<li><a class="step_block inactive" href="apply4.php">Քայլ 4</a></li>
					</ul>
					<form class="apply_form">
						<div class="form_fields">
							<div class="form_subtitle">Ընտրել դասընթացը</div>
							<ul class="radio_list">
								<li>
									<label>
										<input type="radio" name="course" data-validation="required">
										<span class="radio_btn">3D մոդելավորում</span>
									</label>
									<span class="error_hint">Խնդրում ենք ընտրել դասընթացը</span>
								</li>
								<li>
									<label>
										<input type="radio" name="course">
										<span class="radio_btn">Ծրագրավորման հիմունքներ </span>
									</label>
								</li>
								<li>
									<label>
										<input type="radio" name="course">
										<span class="radio_btn">JavaScript սկսնակների համար</span>
									</label>
								</li>
								<li>
									<label>
										<input type="radio" name="course">
										<span class="radio_btn">React JS սկսնակների համար</span>
									</label>
								</li>
							</ul>
						</div>
						<button type="submit" class="validate_btn primary_btn" aria-label="apply">Շարունակել</button>
					</form>
				</div>
			</div>
 		</div>
		<?php
			include 'templates/footer.php'
		?>
		<script src="js/jquery-3.6.0.min.js"></script>
		<script src="js/jquery.form-validator.js"></script>
	 	<script src="js/main.js"></script>
 	</body>
</html>