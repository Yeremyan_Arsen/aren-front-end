  <!DOCTYPE HTML>
<html lang="am">
 	<head>
  		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0, viewport-fit=cover"/>
		<meta name="format-detection" content="telephone=no"/>
		<meta name="description" content="Aren Mehrabyan foundation website">
  		<title>ԱՐԵՆ ՄԵՀՐԱԲՅԱՆ ՀԻՄՆԱԴՐԱՄ</title>
		<link rel="stylesheet" href="css/main.css">
		<link rel="stylesheet" href="css/contacts.css">
		<?php
			include 'templates/favicons.php'
		?>
 	</head>
 	<body>
 		<?php
			include 'templates/header.php'
		?>
 		<div class="content">
			<div class="contacts_section">
				<div class="page_container">
					<h1 class="page_title">Կապ</h1>
					<div class="page_row">
						<div class="contacts_list">
							<div class="contact_block">
								<div class="contact_type">Մեր հասցեն</div>
								<div class="contact_info">Նոր Նորքի 5-րդ զանգված, Ա. Միկոյան 15, Երևան, Հայաստան</div>
							</div>
							<div class="contact_block">
								<div class="contact_type">Էլ․ փոստ</div>
								<div class="contact_info"><a href="mailto:info@arenmehrabyan.or">info@arenmehrabyan.or</a></div>
							</div>
							<div class="contact_block">
								<div class="contact_type">Հեռախոս</div>
								<div class="contact_info"><a href="tel:+37441107148" class="phone_link">(+374) 41-10-71-48</a></div>
							</div>
							<div class="contact_block">
								<div class="contact_type">Սոց․ ցանցեր</div>
								<ul class="socials_list">
									<li><a href="https://www.facebook.com" class="icon_facebook" target="_blank">Facebook</a></li>
									<li><a href="https://www.instagram.com" class="icon_instagram" target="_blank">instagram</a></li>
									<li><a href="https://www.linkedin.com" class="icon_linkedin" target="_blank">linkedin</a></li>
								</ul>
							</div>
						</div>
						<form class="contact_form">
							<div class="field_block">
								<label>
									<span class="label">Անուն ազգանուն</span>
									<input autocomplete="off" type="text" name="name_surname" data-validation="required"/>
                                    <span class="placeholder">Անուն ազգանուն</span>
								</label>
								<span class="error_hint">պարտադիր դաշտ</span>
							</div>
							<div class="field_block">
								<label>
									<span class="label">Էլ. հասցե</span>
									<input autocomplete="off" type="text" name="email" data-validation="email"/>
                                    <span class="placeholder">Էլ. հասցե</span>
								</label>
								<span class="error_hint">
									<span class="standard_hint">պարտադիր դաշտ</span>
									<span class="individual_hint">սխալ էլ. հասցե</span>
								</span>
							</div>
							<div class="field_block">
								<label>
									<span class="label">Թեմա</span>
									<input autocomplete="off" type="text" name="subject" data-validation="required"/>
                                    <span class="placeholder">Թեմա</span>
								</label>
								<span class="error_hint">պարտադիր դաշտ</span>
							</div>
							<div class="field_block">
								<label>
									<span class="label">Փեր նամակը</span>
									<textarea name="message" data-validation="required"></textarea>
                                    <span class="placeholder">Ձեր նամակը</span>
								</label>
								<span class="error_hint">պարտադիր դաշտ</span>
							</div>
							<div class="btn_block">
								<button type="submit" class="validate_btn primary_btn" aria-label="submit">Ուղարկել</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="map_block">
				<script src="https://api-maps.yandex.ru/2.1/?apikey=ваш API-ключ&lang=ru_RU"></script>
				<div id="map" data-coords="40.183010, 44.567649" data-zoom="16" data-markerimage="css/images/favicons/android-icon-48x48.png" data-markersize="48,48" data-defmarker="islands#circleDotIcon" data-defmarkercolor="#8B0029"></div>
			</div>
 		</div>
		<?php
			include 'templates/footer.php'
		?>
		<script src="js/jquery-3.6.0.min.js"></script>
		<script src="js/jquery.form-validator.js"></script>
	 	<script src="js/main.js"></script>
 	</body>
</html>