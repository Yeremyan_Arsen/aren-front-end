  <!DOCTYPE HTML>
<html lang="am">
 	<head>
  		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0, viewport-fit=cover"/>
		<meta name="format-detection" content="telephone=no"/>
		<meta name="description" content="Aren Mehrabyan foundation website">
  		<title>ԱՐԵՆ ՄԵՀՐԱԲՅԱՆ ՀԻՄՆԱԴՐԱՄ</title>
		<link rel="stylesheet" href="css/main.css">
		<link rel="stylesheet" href="css/apply.css">
		<?php
			include 'templates/favicons.php'
		?>
 	</head>
 	<body>
 		<?php
			include 'templates/header.php'
		?>
 		<div class="content">
			<div class="page_title_block">
				<div class="page_container">
					<h1 class="page_title">Դիմել դասընթացին</h1>
				</div>
			</div>
			<div class="apply_content">
				<div class="page_container">
					<ul class="apply_steps">
						<li><a class="step_block" href="apply.php">Քայլ 1</a></li>
						<li><a class="step_block" href="apply2.php">Քայլ 2</a></li>
						<li><a class="step_block" href="apply3.php">Քայլ 3</a></li>
						<li><a class="step_block" href="apply4.php">Քայլ 4</a></li>
						<li class="current_step"><a class="step_block" href="apply_review.php">Քայլ 5</a></li>
					</ul>
					<form class="apply_form">
						<div class="form_fields">
							<ul class="review-list">
								<li>
									<span class="title">Դասընթաց՝</span>
									<span class="value">Ծրագրավորման հիմունքներ</span>
								</li>
								<li>
									<span class="title">Անուն Ազգանուն Հայրանուն`</span>
									<span class="value">Անուն Ազգանուն Հայրանուն</span>
								</li>
								<li>
									<span class="title">Ծննդյան ամսաթիվ`</span>
									<span class="value">12.02.2000թ</span>
								</li>
								<li>
									<span class="title">Բնակավայր`</span>
									<span class="value">ք. Երևան</span>
								</li>
								<li>
									<span class="title">Ընտանեկան կարգավիճակ`</span>
									<span class="value">ամուսնացած</span>
								</li>
								<li>
									<span class="title">Երեխաներ`</span>
									<span class="value">2</span>
								</li>
								<li>
									<span class="title">Հեռախոսահամար`</span>
									<span class="value">094123456</span>
								</li>
								<li>
									<span class="title">Էլ. հասցե`</span>
									<span class="value">example@example.com</span>
								</li>
								<li>
									<span class="title">Սեռ`</span>
									<span class="value">արական</span>
								</li>
								<li>
									<span class="title">Դպրոց`</span>
									<span class="value">Սիամանթոյի անվան թիվ 162 միջնակարգ դպրոց</span>
								</li>
								<li>
									<span class="title">Կրթություն`</span>
									<span class="value">բարձրագույն</span>
								</li>
								<li>
									<span class="title">Օտար լեզուների իմացություն`</span>
									<span class="value">անգլերեն, ռուսերեն</span>
								</li>
								<li>
									<span class="title">Զինվորական մասնագիտություն`</span>
									<span class="value">դասակի հրամանատար</span>
								</li>
								<li>
									<span class="title">ռազմական գործողությունների մասնակցություն`</span>
									<span class="value">այո</span>
								</li>
								<li>
									<span class="title">Աշխատանքային փորձ`</span>
									<span class="value">ոչ</span>
								</li>
								<li>
									<span class="title">Երազանքներ/ծրագրեր/նպատակներ`</span>
									<span class="value">դառնալ ծրագրավորող, ծրագրել գերժամանակակից պաշտպանական համակարգեր</span>
								</li>
							</ul>
						</div>
						
						<button type="submit" class="primary_btn" aria-label="apply">Հաստատել</button>
					</form>
				</div>
			</div>
 		</div>
		<?php
			include 'templates/footer.php'
		?>
		<div class="submit_popup popup_block">
			<div class="popup_inner">
				<div class="popup_container">
					<button class="icon_close popup_close" aria-label="close"></button>
					<div class="page_title">Ձեր դիմումը հաջողությամբ ուղարկված է</div>
					<span class="status_icon icon_checked"></span>
					<!-- <div class="page_title">Տեղի է ունեցել սխալ, խնդրում ենք փորձել մի փոքր ուշ</div>
					<span class="status_icon icon_close"></span> -->
				</div>
			</div>
		</div>
		<script src="js/jquery-3.6.0.min.js"></script>
		<script src="js/jquery.form-validator.js"></script>
	 	<script src="js/main.js"></script>
 	</body>
</html>