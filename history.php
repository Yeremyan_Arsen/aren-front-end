  <!DOCTYPE HTML>
<html lang="am">
 	<head>
  		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0, viewport-fit=cover"/>
		<meta name="format-detection" content="telephone=no"/>
		<meta name="description" content="Aren Mehrabyan foundation website">
  		<title>ԱՐԵՆ ՄԵՀՐԱԲՅԱՆ ՀԻՄՆԱԴՐԱՄ</title>
		<link rel="stylesheet" href="css/jquery.fancybox.css">
		<link rel="stylesheet" href="css/slick.css">
		<link rel="stylesheet" href="css/main.css">
		<link rel="stylesheet" href="css/about.css">
		<?php
			include 'templates/favicons.php'
		?>
 	</head>
 	<body>
 		<?php
			include 'templates/header.php'
		?>
 		<div class="content">
			<div class="page_title_block">
				<div class="page_container">
					<h1 class="page_title">Մեր պատմությունը</h1>
				</div>
			</div>
			
			<div class="history_preview">
				<div class="page_container">
					<div class="info_block">
						<div class="preview_words">Տիեզերքում աստվածային մի ճամփորդ է իմ հոգին.
							<br/>Երկրից անցվոր, երկրի փառքին անհաղորդ է իմ հոգին.
							<br/>Հեռացել է ու վերացել մինչ աստղերը հեռավոր,
							<br/>Վար մնացած մարդու համար արդեն խորթ է իմ հոգին։
							</div>
						<div class="words_author">Հ. Թումանյան</div>
					</div>
					<div class="author_image">
						<img src="images/author_image.jpg" alt="" title="" width="390" height="591"/>
					</div>
				</div>
			</div>

			<div class="historical_overview">
				<h2 class="page_title">Արեն Մեհրաբյանի մասին</h2>
				<div class="history_block">
					<div class="text_block">Արենն իր հարազատների շրջանում հայտնի էր իր անվերջ պրպտող, երազող տեսակով, գիտության նկատմամբ սիրով ու նվիրումով: Ընկերները փաստում են նրա մարդկային արժանիքների մասին, որոնք հետագայում արտահայտվեցին սեփական կյանքն անձնվիրաբար զոհաբերելու վեհ առաքելությամբ:</div>
				</div>
				<div class="history_block">
					<div class="year_block"><span>1997</span></div>
					<div class="text_block">Արեն Մհերի Մեհրաբյանը ծնվել է 1997 թ. սեպտեմբերի 2-ին Երևանում: Միջնակարգ կրթությունը ստացել է Երևանի Անանիա Շիրակացու անվան ճեմարանում: Ճեմարանն ավարտելուց հետո տեղափոխվել է Ամերիկայի Միացյալ Նահանգներ` աստղաֆիզիկոսի մասնագիտություն ստանալու համար: </div>
				</div>
				<div class="history_block">
					<div class="year_block"><span>2018</span></div>
					<div class="text_block">2018 թ. ուսումը կիսատ թողնելով` վերադարձել է Հայաստան` Հայրենիքի հանդեպ պարտքը կատարելու: Ծառայել է Տավուշի մարզի Իջևանի զորամասում որպես հաշվարկի հրետանավոր: Ծառայության ընթացքում ստացել է կրտսեր սերժանտի կոչում և պարգևատրվել պատվոգրերով բարեխիղճ ծառայության, զինվորական կարգապահության և մարտական պատրաստության հարցում ցուցաբերած բարձր արդյունքների համար: Փաստենք, որ Արենը եղել է զորամասի ամենաինտելեկտուալ և հաշվարկի ուժեղագույն հրետանավորը:  </div>
				</div>
				<div class="history_block">
					<div class="year_block"><span>2020</span></div>
					<div class="text_block">2020 թ. սեպտեմբերի 30-ից Արենը մասնակցել է Արցախյան երկրորդ պատերազմի ռազմական գործողություններին: Առաջին իսկ օրերից եղել է մարտի դաշտում` ամենաթեժ կետերում: Զոհվել է հոկտեմբերի 28-ին Մարտակերտում` անօդաչու թռչող սարքի հարվածից: Ծառայակից ընկերների հետ Արենը հասցրել է փրկել երեք վիրավոր հրամանատարների կյանքը, ինչի համար էլ հետմահու արժանացել է &#171;Հերոսի&#187; կոչում, պարգևատրվել է &#171;Մարտական ծառայության&#187; մեդալով և &#171;Հայրենիքի պաշտպանության&#187; շքանշանով:
					</div>
				</div>
				<div class="history_block">
					<div class="year_block"></div>
					<div class="text_block">Արեն Մեհրաբյանն անչափ հայրենասեր էր, մարդասեր, ծնողասեր ու ընկերասեր: Շատ էր սիրում Հայաստանը: Բոլոր երիտասարդների նման Արենն էլ ուներ երազանքներ, առավելապես ձգտում էր իր կարևորագույն ներդրումն ունենալ գիտության զարգացման ոլորտում: Ուներ ծրագրեր: Արենը երազանքները նպատակ էր դարձնում, նպատակները` ծրագրեր ու որոշում դրանք անպայման կյանքի կոչել: Տիեզերքը Արենի մեջ էր: Արենը Տիեզերք էր:</div>
				</div>
			</div>

			<div class="images_slider">
				<div class="page_container">
					<div class="slider_list">
						<div class="slide_block">
							<a href="images/image-25.jpg" data-fancybox="gallery" class="image_block">
								<img src="images/image-25.jpg" alt="" title="" width="260" height="360"/>
								gallery image name
							</a>
						</div>
						<div class="slide_block">
							<a href="images/image-28.jpg" data-fancybox="gallery" class="image_block">
								<img src="images/image-28.jpg" alt="" title="" width="260" height="360"/>
								gallery image name
							</a>
						</div>
						<div class="slide_block">
							<a href="images/image-29.jpg" data-fancybox="gallery" class="image_block">
								<img src="images/image-29.jpg" alt="" title="" width="260" height="360"/>
								gallery image name
							</a>
						</div>
						<div class="slide_block">
							<a href="images/image-30.jpg" data-fancybox="gallery" class="image_block">
								<img src="images/image-30.jpg" alt="" title="" width="260" height="360"/>
								gallery image name
							</a>
						</div>
						<div class="slide_block">
							<a href="images/image-25.jpg" data-fancybox="gallery" class="image_block">
								<img src="images/image-25.jpg" alt="" title="" width="260" height="360"/>
								gallery image name
							</a>
						</div>
						<div class="slide_block">
							<a href="images/image-28.jpg" data-fancybox="gallery" class="image_block">
								<img src="images/image-28.jpg" alt="" title="" width="260" height="360"/>
								gallery image name
							</a>
						</div>
						<div class="slide_block">
							<a href="images/image-29.jpg" data-fancybox="gallery" class="image_block">
								<img src="images/image-29.jpg" alt="" title="" width="260" height="360"/>
								gallery image name
							</a>
						</div>
						<div class="slide_block">
							<a href="images/image-30.jpg" data-fancybox="gallery" class="image_block">
								<img src="images/image-30.jpg" alt="" title="" width="260" height="360"/>
								gallery image name
							</a>
						</div>
					</div>
				</div>
			</div>

			<div class="video_section">
				<div class="section_inner">
					<div class="section_description">«Արեն Մեհրաբյան» բարեգործական հիմնադրամ․ իրականացնելով Արենի «աերոտիեզերական» երազանքները</div>
					<iframe width="900" height="506" src="https://www.youtube.com/embed/FC2TOswbijU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</div>
			</div>
 		</div>
		<?php
			include 'templates/footer.php'
		?>
		<script src="js/jquery-3.6.0.min.js"></script>
 		<script src="js/slick.js"></script>
 		<script src="js/jquery.fancybox.js"></script>
	 	<script src="js/main.js"></script>
 	</body>
</html>