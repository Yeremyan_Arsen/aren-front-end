<!DOCTYPE HTML>
<html lang="am">
<head>
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0, viewport-fit=cover"/>
    <meta name="format-detection" content="telephone=no"/>
    <meta name="description" content="Aren Mehrabyan foundation website">
    <title>ԱՐԵՆ ՄԵՀՐԱԲՅԱՆ ՀԻՄՆԱԴՐԱՄ</title>
    <link rel="stylesheet" href="css/main.css">
    <?php
    include 'templates/favicons.php'
    ?>
</head>
<body>
<?php
include 'templates/header.php'
?>
<div class="content">
    <div class="page_title_block">
        <div class="page_container">
            <h1 class="page_title">Ֆինանսական ցուցանիշներ</h1>
        </div>
    </div>
    <!--<img src="css/images/pdf.svg" width="46" height="46" alt="" title=""/>-->
    <div class="financial_section">
        <div class="page_container">
            <ul class="financial_list">
                <li>
                    <a href="https://www.coaf.org/wp-content/uploads/2023/07/COAF-Form-990.pdf" target="_blank"
                       class="icon_pdf">
                        COAF 990 ձև
                    </a>
                </li>
                <li>
                    <a href="https://www.coaf.org/wp-content/uploads/2023/07/Financial-statements-2022-Armenia-ENG.pdf"
                       class="icon_pdf" target="_blank">
                        2022 Ֆինանսական հաշվետվություն, Հայաստան
                    </a>
                </li>
                <li>
                    <a href="https://www.coaf.org/wp-content/uploads/2023/07/Financial-statements-2022-Armenia-ENG.pdf"
                       class="icon_pdf" target="_blank">
                        2020 Ֆինանսական հաշվետվություն, ԱՄՆ
                    </a>
                </li>
                <li>
                    <a href="https://www.coaf.org/wp-content/uploads/2023/07/COAF-Form-990.pdf" target="_blank"
                       class="icon_pdf">
                        COAF 990 ձև
                    </a>
                </li>
                <li>
                    <a href="https://www.coaf.org/wp-content/uploads/2023/07/Financial-statements-2022-Armenia-ENG.pdf"
                       class="icon_pdf" target="_blank">
                        2022 Ֆինանսական հաշվետվություն, Հայաստան
                    </a>
                </li>
                <li>
                    <a href="https://www.coaf.org/wp-content/uploads/2023/07/Financial-statements-2022-Armenia-ENG.pdf"
                       class="icon_pdf" target="_blank">
                        2020 Ֆինանսական հաշվետվություն, ԱՄՆ
                    </a>
                </li>
                <li>
                    <a href="https://www.coaf.org/wp-content/uploads/2023/07/COAF-Form-990.pdf" target="_blank"
                       class="icon_pdf">
                        COAF 990 ձև
                    </a>
                </li>
                <li>
                    <a href="https://www.coaf.org/wp-content/uploads/2023/07/Financial-statements-2022-Armenia-ENG.pdf"
                       class="icon_pdf" target="_blank">
                        2022 Ֆինանսական հաշվետվություն, Հայաստան
                    </a>
                </li>
                <li>
                    <a href="https://www.coaf.org/wp-content/uploads/2023/07/Financial-statements-2022-Armenia-ENG.pdf"
                       class="icon_pdf" target="_blank">
                        2020 Ֆինանսական հաշվետվություն, ԱՄՆ
                    </a>
                </li>
                <li>
                    <a href="https://www.coaf.org/wp-content/uploads/2023/07/Financial-statements-2022-Armenia-ENG.pdf"
                       class="icon_pdf" target="_blank">
                        2020 Ֆինանսական հաշվետվություն, ԱՄՆ
                    </a>
                </li>
                <li>
                    <a href="https://www.coaf.org/wp-content/uploads/2023/07/COAF-Form-990.pdf" target="_blank"
                       class="icon_pdf">
                        COAF 990 ձև
                    </a>
                </li>
                <li>
                    <a href="https://www.coaf.org/wp-content/uploads/2023/07/Financial-statements-2022-Armenia-ENG.pdf"
                       class="icon_pdf" target="_blank">
                        2020 Ֆինանսական հաշվետվություն, ԱՄՆ
                    </a>
                </li>
                <li>
                    <a href="https://www.coaf.org/wp-content/uploads/2023/07/COAF-Form-990.pdf" target="_blank"
                       class="icon_pdf">
                        COAF 990 ձև
                    </a>
                </li>
                <li>
                    <a href="https://www.coaf.org/wp-content/uploads/2023/07/Financial-statements-2022-Armenia-ENG.pdf"
                       class="icon_pdf" target="_blank">
                        2020 Ֆինանսական հաշվետվություն, ԱՄՆ
                    </a>
                </li>
                <li>
                    <a href="https://www.coaf.org/wp-content/uploads/2023/07/COAF-Form-990.pdf" target="_blank"
                       class="icon_pdf">
                        COAF 990 ձև
                    </a>
                </li>
                <li>
                    <a href="https://www.coaf.org/wp-content/uploads/2023/07/Financial-statements-2022-Armenia-ENG.pdf"
                       class="icon_pdf" target="_blank">
                        2020 Ֆինանսական հաշվետվություն, ԱՄՆ
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
<?php
include 'templates/footer.php'
?>
<script src="js/jquery-3.6.0.min.js"></script>
<script src="js/main.js"></script>
</body>
</html>