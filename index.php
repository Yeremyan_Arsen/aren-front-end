  <!DOCTYPE HTML>
<html lang="am">
 	<head>
  		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0, viewport-fit=cover"/>
		<meta name="format-detection" content="telephone=no"/>
		<meta name="description" content="Aren Mehrabyan foundation website">
  		<title>ԱՐԵՆ ՄԵՀՐԱԲՅԱՆ ՀԻՄՆԱԴՐԱՄ</title>
		<link rel="stylesheet" href="css/slick.css">
		<link rel="stylesheet" href="css/main.css">
		<link rel="stylesheet" href="css/index.css">
		<?php
			include 'templates/favicons.php'
		?>
 	</head>
 	<body>
 		<?php
			include 'templates/header.php'
		?>
 		<div class="content">
			<div class="main_slider">
				<div class="slide_block">
					<video preload="" playsinline="" muted="" width="1920" height="960">
						<source src="video/video.mp4" type="video/mp4">
					</video>
					<div class="info_block">
						<div class="page_container">
							<div class="description_block">Ուզում էի ստեղծել Հայաստանում աերոտիեզերական մեքենաշինական արտադրություն, որը հիմա դու ես անելու…</div>
							<a href="donate.php" class="primary_btn">Նվիրաբերել</a>
						</div>
					</div>
				</div>
				<div class="slide_block">
					<video preload playsinline="" muted="" width="1920" height="960">
						<source src="video/video1.mp4" type="video/mp4">
					</video>
					<div class="info_block">
						<div class="page_container">
							<div class="description_block">Նկարագրության տեքստ, ոչ ավելի քան 2 տող</div>
							<a href="donate.php" class="primary_btn">Նվիրաբերել</a>
						</div>
					</div>
				</div>
				<div class="slide_block">
					<video preload playsinline="" muted="" width="1920" height="960">
						<source src="video/video2.mp4" type="video/mp4">
					</video>
					<div class="info_block">
						<div class="page_container">
							<div class="description_block">Կարճ նկարագրություն</div>
							<a href="donate.php" class="primary_btn">Նվիրաբերել</a>
						</div>
					</div>
				</div>
				<div class="slide_block">
					<video preload playsinline="" muted="" width="1920" height="960">
						<source src="video/video3.mp4" type="video/mp4">
					</video>
					<div class="info_block">
						<div class="page_container">
							<div class="description_block">Կարճ տեքստ տվյալ տեսանյութի մասին</div>
							<a href="donate.php" class="primary_btn">Նվիրաբերել</a>
						</div>
					</div>
				</div>
				<div class="slide_block">
					<video preload playsinline="" muted="" width="1920" height="960">
						<source src="video/video4.mp4" type="video/mp4">
					</video>
					<div class="info_block">
						<div class="page_container">
							<div class="description_block">Նկարագրության տեքստ, ոչ ավելի քան 2 տող</div>
							<a href="donate.php" class="primary_btn">Նվիրաբերել</a>
						</div>
					</div>
				</div>
				<div class="slide_block">
					<video preload playsinline="" muted="" width="1920" height="960">
						<source src="video/video5.mp4" type="video/mp4">
					</video>
					<div class="info_block">
						<div class="page_container">
							<div class="description_block">Կարճ նկարագրություն</div>
							<a href="donate.php" class="primary_btn">Նվիրաբերել</a>
						</div>
					</div>
				</div>
				<div class="slide_block">
					<video preload playsinline="" muted="" width="1920" height="960">
						<source src="video/video6.mp4" type="video/mp4">
					</video>
					<div class="info_block">
						<div class="page_container">
							<div class="description_block">Կարճ տեքստ տվյալ տեսանյութի մասին</div>
							<a href="donate.php" class="primary_btn">Նվիրաբերել</a>
						</div>
					</div>
				</div>
			</div>

			<div class="actions_section">
				<div class="page_container">
					<ul>
						<li>
							<img src="css/images/apply.svg" alt="" title="" width="101" height="122"/>
							<a href="apply.php" class="secondary_btn">Դիմել դասընթացին</a>
						</li>
						<li>
							<img src="css/images/heart_in_hands.svg" alt="" title="" width="145" height="122"/>
							<a href="donate.php" class="primary_btn">Նվիրաբերել</a>
						</li>
						<li>
							<img src="css/images/cooperate.svg" alt="" title="" width="174" height="122"/>
							<a href="cooperate.php" class="secondary_btn">Համագործակցել</a>
						</li>
					</ul>
				</div>
			</div>

			<div class="slider_section">
				<div class="page_container">
					<h2 class="section_title">ԿՐԹԱԿԱՆ ԾՐԱԳՐԵՐ</h2>
					<div class="news_slider">
						<div class="slide_block">
							<a href="" class="news_block">
								<div class="image_block">
									<img src="images/news_image1.jpg" alt="" title="" width="375" height="260"/>
								</div>
								<div class="date_block">Մարտի 23</div>
								<div class="title_block">Մարտի 23-ին, 2022թ. մեկնարկեց Ծրագրավորման դասընթացների նոր փուլը</div>
								<a class="more_link" href="program_inner.php">ավելին</a>
							</a>
						</div>
						<div class="slide_block">
							<a href="" class="news_block">
								<div class="image_block">
									<img src="images/news_image2.jpg" alt="" title="" width="375" height="260"/>
								</div>
								<div class="date_block">Ապրիլի 30</div>
								<div class="title_block">Տեխնոլոգիական կրթության ակադեմիային (TTA) և FESTO ընկերության լիազորված ներկայացուցիչ Կոմպատեր ՍՊԸ-ին, ինչպես նաև</div>
								<a class="more_link" href="program_inner.php">ավելին</a>
							</a>
						</div>
						<div class="slide_block">
							<a href="" class="news_block">
								<div class="image_block">
									<img src="images/news_image3.jpg" alt="" title="" width="375" height="260"/>
								</div>
								<div class="date_block">Մայիսի 6</div>
								<div class="title_block">Թորոնթոյի Համազգայինի թատերասրահում կներկայացնեմ իմ « Խոստում »</div>
								<a class="more_link" href="program_inner.php">ավելին</a>
							</a>
						</div>
						<div class="slide_block">
							<a href="" class="news_block">
								<div class="image_block">
									<img src="images/news_image1.jpg" alt="" title="" width="375" height="260"/>
								</div>
								<div class="date_block">Մարտի 23</div>
								<div class="title_block">Մարտի 23-ին, 2022թ. մեկնարկեց Ծրագրավորման դասընթացների նոր փուլը</div>
								<a class="more_link" href="program_inner.php">ավելին</a>
							</a>
						</div>
						<div class="slide_block">
							<a href="" class="news_block">
								<div class="image_block">
									<img src="images/news_image2.jpg" alt="" title="" width="375" height="260"/>
								</div>
								<div class="date_block">Ապրիլի 30</div>
								<div class="title_block">Տեխնոլոգիական կրթության ակադեմիային (TTA) և FESTO ընկերության լիազորված ներկայացուցիչ Կոմպատեր ՍՊԸ-ին, ինչպես նաև</div>
								<a class="more_link" href="program_inner.php">ավելին</a>
							</a>
						</div>
						<div class="slide_block">
							<a href="" class="news_block">
								<div class="image_block">
									<img src="images/news_image3.jpg" alt="" title="" width="375" height="260"/>
								</div>
								<div class="date_block">Մայիսի 6</div>
								<div class="title_block">Թորոնթոյի Համազգայինի թատերասրահում կներկայացնեմ իմ « Խոստում »</div>
								<a class="more_link" href="program_inner.php">ավելին</a>
							</a>
						</div>
						<div class="slide_block">
							<a href="" class="news_block">
								<div class="image_block">
									<img src="images/news_image1.jpg" alt="" title="" width="375" height="260"/>
								</div>
								<div class="date_block">Մարտի 23</div>
								<div class="title_block">Մարտի 23-ին, 2022թ. մեկնարկեց Ծրագրավորման դասընթացների նոր փուլը</div>
								<a class="more_link" href="program_inner.php">ավելին</a>
							</a>
						</div>
						<div class="slide_block">
							<a href="" class="news_block">
								<div class="image_block">
									<img src="images/news_image2.jpg" alt="" title="" width="375" height="260"/>
								</div>
								<div class="date_block">Ապրիլի 30</div>
								<div class="title_block">Տեխնոլոգիական կրթության ակադեմիային (TTA) և FESTO ընկերության լիազորված ներկայացուցիչ Կոմպատեր ՍՊԸ-ին, ինչպես նաև</div>
								<a class="more_link" href="program_inner.php">ավելին</a>
							</a>
						</div>
						<div class="slide_block">
							<a href="" class="news_block">
								<div class="image_block">
									<img src="images/news_image3.jpg" alt="" title="" width="375" height="260"/>
								</div>
								<div class="date_block">Մայիսի 6</div>
								<div class="title_block">Թորոնթոյի Համազգայինի թատերասրահում կներկայացնեմ իմ « Խոստում »</div>
								<a class="more_link" href="program_inner.php">ավելին</a>
							</a>
						</div>
					</div>
				</div>
				
			</div>

			<div class="dream_section">
				<div class="diagram_section" style="background-image: url('images/dream_section_bg.jpg')">
					<div class="page_container">
						<div class="section_inner">
							<svg  x="0px" y="0px" viewBox="0 0 1072 731" style="enable-background:new 0 0 1072 731;" xml:space="preserve">
								<g class="branches">
									<path fill="none" stroke="#6B7C89" d="M152.7,297.4L494.9,393"/>
									<path fill="none" stroke="#6B7C89" d="M921.5,467l-369.4-85.1"/>
									<circle fill="#6B7C89" cx="154" cy="297.1" r="4"/>
									<circle fill="#6B7C89" cx="921.1" cy="467.1" r="4"/>
									<path fill="none" stroke="#6B7C89" d="M100.4,377.9l395.8,20.5"/>
									<path fill="none" stroke="#6B7C89" d="M947.5,326.5L550,376.7"/>
									<path fill="#6B7C89" d="M102.1,381.1c-2.2-0.2-3.8-2.1-3.7-4.3c0.2-2.2,2.1-3.8,4.3-3.7c2.2,0.2,3.8,2.1,3.7,4.3
										C106.2,379.6,104.3,381.2,102.1,381.1z"/>
									<path fill="#6B7C89" d="M947,322.3c2.2-0.1,4.1,1.5,4.3,3.7c0.1,2.2-1.5,4.1-3.7,4.3c-2.2,0.1-4.1-1.5-4.3-3.7
										C943.1,324.3,944.8,322.4,947,322.3z"/>
									<path fill="none" stroke="#6B7C89" d="M309,69l216.5,265.5"/>
									<circle fill="#6B7C89" cx="310.5" cy="70.5" r="4"/>
									<path fill="none" stroke="#6B7C89" d="M730,681.5l-193.5-271"/>
									<circle fill="#6B7C89" cx="729.3" cy="682.3" r="4"/>
									<circle fill="#6B7C89" cx="823.6" cy="642.2" r="4"/>
									<path fill="none" stroke="#6B7C89" d="M218.6,565.9L524,391.8"/>
									<circle fill="#6B7C89" cx="219.2" cy="564.7" r="4"/>
									<path fill="none" stroke="#6B7C89" d="M821.6,640.9L537.5,412"/>
									<path fill="none" stroke="#6B7C89" d="M280.5,674.2l254.6-297.4"/>
									<path fill="#6B7C89" d="M284,674.6c-1.4,1.7-3.9,2-5.6,0.6c-1.7-1.4-2-3.9-0.6-5.6c1.4-1.7,3.9-2,5.6-0.6
										C285.2,670.4,285.4,672.9,284,674.6z"/>
									<path fill="none" stroke="#6B7C89" d="M810.5,102L556.4,337"/>
									<path fill="none" stroke="#6B7C89" d="M975.5,95l-438,279"/>
									<path fill="none" stroke="#6B7C89" d="M535.5,372L125,88.5"/>
									<path fill="#6B7C89" d="M808.2,98.4c1.8-1.3,4.3-0.9,5.6,0.9c1.3,1.8,0.9,4.3-0.9,5.6c-1.8,1.3-4.3,0.9-5.6-0.9
										C806.1,102.2,806.4,99.7,808.2,98.4z"/>
									<path fill="#6B7C89" d="M123.2,85.4c1.8-1.3,4.3-0.9,5.6,0.9c1.3,1.8,0.9,4.3-0.9,5.6c-1.8,1.3-4.3,0.9-5.6-0.9
										C121.1,89.2,121.4,86.7,123.2,85.4z"/>
									<path fill="#6B7C89" d="M972.2,93.4c1.8-1.3,4.3-0.9,5.6,0.9c1.3,1.8,0.9,4.3-0.9,5.6c-1.8,1.3-4.3,0.9-5.6-0.9
										C970.1,97.2,970.4,94.7,972.2,93.4z"/>
								</g>
								<g class="arrows">
									<path fill="#ffffff" d="M212.8,201.2l6,9.9l5.6-10.1L212.8,201.2z M220.2,206.4l313,171.7l1-1.8l-313-171.7L220.2,206.4z"/>
									<path fill="#ffffff" d="M854.4,202.6l-11.5-0.3l5.5,10.2L854.4,202.6z M846,206L531.6,375l0.9,1.8l314.4-169L846,206z"/>
									<path fill="#ffffff" d="M535,731l5.8-10h-11.5L535,731z M536,722l0-357h-2l0,357H536z"/>
								</g>
								<circle fill="none" stroke="#8B0029" class="red_circle" stroke-width="3" cx="535" cy="365" r="338"/>
								<circle fill="none" stroke="#6B7C89" cx="535" cy="365" r="232.5"/>
								<g class="sectors">
									<a href="project_inner.php">
										<path fill="#27496D" d="M785,226.8c-24.8-43-66.8-83.5-109.8-108.3s-91.6-37.9-141.1-37.9s-98.2,13.1-141.2,37.9C350,143.2,309.8,185,285,228l0.3,0.2l249.2,135.3l250-136.5L785,226.8z"/>
										Գիտակրթական ակադեմիա, թանգարան -լաբորատորիա
									</a>
									<a href="project_inner.php">
										<path fill="#27496D" d="M272,248.5c-24.8,42.8-34.1,87-34,136.5c0,155.8,131.8,282,287.5,282V385L272.3,248.7L272,248.5z"/>
										Աերոտիեզերական ծրագիր
									</a>
									<a href="project_inner.php">
										<path fill="#27496D" d="M796,248.3c24.8,42.8,34.1,87,34,136.5c0,155.8-131.3,282-287,282v-282l252.7-136.3L796,248.3z"/>
										Կրթական ծրագրեր
									</a>
								</g>
							</svg>
							<div class="diagram_name"><span>ԱՐԵՆԻ ԵՐԱԶԱՆՔԸ</span></div>
							<span class="sector_name top_sector">Գիտակրթական ակադեմիա, թանգարան -լաբորատորիա</span>
							<span class="sector_name left_sector">Աերոտիեզերական ծրագիր</span>
							<span class="sector_name right_sector">Կրթական ծրագրեր</span>
						</div>
					</div>
				</div>
				<div class="includes_section">
					<div class="program_block">
						<div class="program_name">Գիտակրթական ակադեմիա,<br/>թանգարան -լաբորատորիա</div>
						<ul class="program_includes">
							<li>
								<div class="count_block">36</div>
								<div class="name_block">Աշխատանքի ընդունված</div>
							</li>
							<li>
								<div class="count_block">120</div>
								<div class="name_block">Որակավորված մասնագետ</div>
							</li>
							<li>
								<div class="count_block">23</div>
								<div class="name_block">Դասընթաց</div>
							</li>
							<li>
								<div class="count_block">36</div>
								<div class="name_block">Աշխատանքի ընդունված</div>
							</li>
						</ul>
					</div>
					<div class="program_block">
						<div class="program_name">Աերոտիեզերական ծրագիր</div>
						<ul class="program_includes">
							<li>
								<div class="count_block">23</div>
								<div class="name_block">Դասընթաց</div>
							</li>
							<li>
								<div class="count_block">36</div>
								<div class="name_block">Աշխատանքի ընդունված</div>
							</li>
							<li>
								<div class="count_block">120</div>
								<div class="name_block">Որակավորված մասնագետ</div>
							</li>
							<li>
								<div class="count_block">23</div>
								<div class="name_block">Դասընթաց</div>
							</li>
						</ul>
					</div>
					<div class="program_block">
						<div class="program_name">Կրթական ծրագրեր</div>
						<ul class="program_includes">
							<li>
								<div class="count_block">36</div>
								<div class="name_block">Աշխատանքի ընդունված</div>
							</li>
							<li>
								<div class="count_block">23</div>
								<div class="name_block">Դասընթաց</div>
							</li>
							<li>
								<div class="count_block">23</div>
								<div class="name_block">Դասընթաց</div>
							</li>
							<li>
								<div class="count_block">36</div>
								<div class="name_block">Աշխատանքի ընդունված</div>
							</li>
						</ul>
					</div>
				</div>
			</div>

			<div class="slider_section">
				<div class="page_container">
					<h2 class="section_title">ԲԼՈԳ</h2>
					<div class="news_slider">
						<div class="slide_block">
							<a href="" class="news_block">
								<div class="image_block">
									<img src="images/news_image1.jpg" alt="" title="" width="375" height="260"/>
								</div>
								<div class="title_block">Մարտի 23-ին, 2022թ. մեկնարկեց Ծրագրավորման դասընթացների նոր փուլը</div>
								<a class="more_link" href="program_inner.php">ավելին</a>
							</a>
						</div>
						<div class="slide_block">
							<a href="" class="news_block">
								<div class="image_block">
									<img src="images/news_image2.jpg" alt="" title="" width="375" height="260"/>
								</div>
								<div class="title_block">Տեխնոլոգիական կրթության ակադեմիային (TTA) և FESTO ընկերության լիազորված ներկայացուցիչ Կոմպատեր ՍՊԸ-ին, ինչպես նաև</div>
								<a class="more_link" href="program_inner.php">ավելին</a>
							</a>
						</div>
						<div class="slide_block">
							<a href="" class="news_block">
								<div class="image_block">
									<img src="images/news_image3.jpg" alt="" title="" width="375" height="260"/>
								</div>
								<div class="title_block">Թորոնթոյի Համազգայինի թատերասրահում կներկայացնեմ իմ « Խոստում »</div>
								<a class="more_link" href="program_inner.php">ավելին</a>
							</a>
						</div>
						<div class="slide_block">
							<a href="" class="news_block">
								<div class="image_block">
									<img src="images/news_image1.jpg" alt="" title="" width="375" height="260"/>
								</div>
								<div class="title_block">Մարտի 23-ին, 2022թ. մեկնարկեց Ծրագրավորման դասընթացների նոր փուլը</div>
								<a class="more_link" href="program_inner.php">ավելին</a>
							</a>
						</div>
						<div class="slide_block">
							<a href="" class="news_block">
								<div class="image_block">
									<img src="images/news_image2.jpg" alt="" title="" width="375" height="260"/>
								</div>
								<div class="title_block">Տեխնոլոգիական կրթության ակադեմիային (TTA) և FESTO ընկերության լիազորված ներկայացուցիչ Կոմպատեր ՍՊԸ-ին, ինչպես նաև</div>
								<a class="more_link" href="program_inner.php">ավելին</a>
							</a>
						</div>
						<div class="slide_block">
							<a href="" class="news_block">
								<div class="image_block">
									<img src="images/news_image3.jpg" alt="" title="" width="375" height="260"/>
								</div>
								<div class="title_block">Թորոնթոյի Համազգայինի թատերասրահում կներկայացնեմ իմ « Խոստում »</div>
								<a class="more_link" href="program_inner.php">ավելին</a>
							</a>
						</div>
						<div class="slide_block">
							<a href="" class="news_block">
								<div class="image_block">
									<img src="images/news_image1.jpg" alt="" title="" width="375" height="260"/>
								</div>
								<div class="title_block">Մարտի 23-ին, 2022թ. մեկնարկեց Ծրագրավորման դասընթացների նոր փուլը</div>
								<a class="more_link" href="program_inner.php">ավելին</a>
							</a>
						</div>
						<div class="slide_block">
							<a href="" class="news_block">
								<div class="image_block">
									<img src="images/news_image2.jpg" alt="" title="" width="375" height="260"/>
								</div>
								<div class="title_block">Տեխնոլոգիական կրթության ակադեմիային (TTA) և FESTO ընկերության լիազորված ներկայացուցիչ Կոմպատեր ՍՊԸ-ին, ինչպես նաև</div>
								<a class="more_link" href="program_inner.php">ավելին</a>
							</a>
						</div>
						<div class="slide_block">
							<a href="" class="news_block">
								<div class="image_block">
									<img src="images/news_image3.jpg" alt="" title="" width="375" height="260"/>
								</div>
								<div class="title_block">Թորոնթոյի Համազգայինի թատերասրահում կներկայացնեմ իմ « Խոստում »</div>
								<a class="more_link" href="program_inner.php">ավելին</a>
							</a>
						</div>
					</div>
				</div>
				
			</div>
 		</div>
		<?php
			include 'templates/footer.php'
		?>
		<script src="js/jquery-3.6.0.min.js"></script>
 		<script src="js/slick.js"></script>
	 	<script src="js/main.js"></script>
 	</body>
</html>