  <!DOCTYPE HTML>
<html lang="am">
 	<head>
  		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0, viewport-fit=cover"/>
		<meta name="format-detection" content="telephone=no"/>
		<meta name="description" content="Aren Mehrabyan foundation website">
  		<title>ԱՐԵՆ ՄԵՀՐԱԲՅԱՆ ՀԻՄՆԱԴՐԱՄ</title>
		<link rel="stylesheet" href="css/main.css">
		<link rel="stylesheet" href="css/programs.css">
		<?php
			include 'templates/favicons.php'
		?>
 	</head>
 	<body>
 		<?php
			include 'templates/header.php'
		?>
 		<div class="content">
			<div class="page_head_block">
				<div class="head_inner">
					<div class="page_container">
						<div class="info_block">
							<h1 class="page_title">Գիտակրթական ակադեմիա,  <br/>թանգարան - լաբորատորիա</h1>
						</div>
						<div class="image_block">
							<img src="images/academy_interior.jpg" alt="" title="" width="1160" height="700"/>
						</div>
					</div>
				</div>
			</div>

			<div class="program_purpose">
				<div class="page_container">
					<h2 class="page_title">Ակադեմիայի նպատակը</h2>
					<div class="info_block">Գիտակրթական ակադեմիայի նպատակն է կրթել սերունդներին՝ սեր առաջացնելով դեպի բնագիտական (ֆիզիկա, քիմիա և կենսաբանություն) առարկաները՝ կարևորելով վաղվա սերնդի գիտատեխնիկական կրթության ապահովումը: Այս ծրագրի շրջանակում նախատեսում ենք կառուցել ֆիզիկայի, քիմիայի և կենսաբանության բնագավառին առնչվող թանգարան-լաբորատորիաներ, որոնց միջոցով մանկահասակ երեխաները, 
					<br/>դպրոցականները, ուսանողները, ուսուցիչները, դասախոսները, գիտնականները, ինչպես նաև` թեմայով հետաքրքրված անհատները հնարավորություն կունենան տեղում տեսնելու և մասնակցելու հայտնի գիտնականների կատարած փորձերին և ապրելու նույն զգացումները և բերկրանքը, որ ժամանակին ունեցել են այդ հայտնագործությունների, գյուտերի հեղինակները:
					</div>
				</div>
			</div>

			<div class="info_section">
				<div class="page_container">
					<h2 class="page_title">Գաղափարներ</h2>
					<div class="info_block">
						<div class="inner_block">
							<div class="text_block">
								<ul>
									<li>Թանգարան-լաբորատորիայի շենքի կառուցվածքը նման է մարդկության մտքի անցած էվոլուցիայի ճանապարհին: Մտնելով շենք` մարդը քայլելով անցնում է գիտական մտքի զարգացման ճանապարհով` մեկ առ մեկ ականատես և մասնակից լինելով գիտական փորձերին, որոնք կարևոր առաջընթաց են ապահովել մարդկության պատմության մեջ: 
									</li>
									<li>Թանգարան-լաբորատորիան կահավորված է տվյալ ժամանակի շունչն արտացոլող միջավայրով, որն այցելուի մոտ ստեղծում է լիակատար պատկերացում` կատարված հայտնագործության պայմանների վերաբերյալ: </li>
									<li>Թանգարան-լաբորատորիայում փորձերը ներկայացված են լինելու օրիգինալ սարքերի կրկնօրինակների միջոցով, ճիշտ նույն կերպ, ինչպես գիտնականը մտածել և իրագործել է: 
									</li>
									<li>Թանգարան-լաբորատորիայի շենքի կառուցվածքի հիմք է հանդիսանալու Բաբելոնյան աշտարակի համանման մի ժամանակակից կառույց, որը արտաքինից արտահայտելու է մարդկության մտքի ձգտումը դեպի կատարելություն` դեպի աստվածային արարչագործություն:</li>
									<li> Թանգարան-լաբորատորիայի շենքը միշտ անավարտ է` այն անընդհատ կառուցվում է: Նույն կերպ՝ մարդկային մտքի զարգացումն է անավարտ, շարունակելի…</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="image_block">
						<img src="images/academy_exterior.jpg" alt="" title="" width="570" height="590"/>
					</div>
				</div>
			</div>

			<div class="visitors_section">
				<div class="title_block">
					<div class="page_container">
						<h2 class="page_title">Ովքե՞ր են մեր այցելուները</h2>
					</div>
				</div>
				<img src="images/visitors1.jpg" alt="" title="" width="960" height="665"/>
				<img src="images/visitors2.jpg" alt="" title="" width="960" height="665"/>
				<div class="info_block">
					<div class="page_container">
						<div class="info_inner">
							<div class="inner_block">Թանգարան-լաբորատորիայի այցելուներն են մանկահասակ երեխաները, դպրոցականները, ուսանողները, ուսուցիչները, դասախոսները, գիտության ոլորտում աշխատողները, գիտնականները, ինչպես նաև` թեմայով հետաքրքրված անհատները:</div>
							<div class="inner_block">Ուսուցիչները և դասախոսները հնարավորություն կունենան խորապես ուսումնասիրելու դասավանդվող տեսությունները և համադրելու դրանք կատարված փորձերի էվոլուցիոն գործոնների հետ:</div>
							<div class="inner_block">Երեխաները կկարողանան մասնակցել կատարվող փորձերին` իրենց համար բացահայտելով գիտության հրաշքը:</div>
							<div class="inner_block">Երեխաները կկարողանան մասնակցել կատարվող փորձերին` իրենց համար բացահայտելով գիտության հրաշքը:</div>
							<div class="inner_block">Ուսանողները կկարողանան կատարել զանազան փորձեր` ամրապնդելու իրենց տեսական գիտելիքները:</div>
							<div class="inner_block">Թանգարան-լաբորատորիայի դռները բաց են բոլոր այցելուների առաջ` հետաքրքիր ժամանցի և իրական գիտության հետ շփվելու համար:</div>
						</div>
					</div>
				</div>
			</div>
	
 		</div>
		<?php
			include 'templates/footer.php'
		?>
		<script src="js/jquery-3.6.0.min.js"></script>
	 	<script src="js/main.js"></script>
 	</body>
</html>