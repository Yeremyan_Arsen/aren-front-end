  <!DOCTYPE HTML>
<html lang="am">
 	<head>
  		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0, viewport-fit=cover"/>
		<meta name="format-detection" content="telephone=no"/>
		<meta name="description" content="Aren Mehrabyan foundation website">
  		<title>ԱՐԵՆ ՄԵՀՐԱԲՅԱՆ ՀԻՄՆԱԴՐԱՄ</title>
		<link rel="stylesheet" href="css/main.css">
		<link rel="stylesheet" href="css/apply.css">
		<?php
			include 'templates/favicons.php'
		?>
 	</head>
 	<body>
 		<?php
			include 'templates/header.php'
		?>
 		<div class="content">
			<div class="page_title_block">
				<div class="page_container">
					<h1 class="page_title">Դիմել դասընթացին</h1>
				</div>
			</div>
			<div class="apply_content">
				<div class="page_container">
					<ul class="apply_steps">
						<li><a class="step_block" href="apply.php">Քայլ 1</a></li>
						<li><a class="step_block" href="apply2.php">Քայլ 2</a></li>
						<li><a class="step_block" href="apply3.php">Քայլ 3</a></li>
						<li class="current_step"><a class="step_block" href="apply4.php">Քայլ 4</a></li>
					</ul>
					<form class="apply_form">
						<div class="form_fields">
							<div class="form_subtitle">Աշխատանքային փորձ</div>
							<div class="field_block">
								<label>
									<textarea rows="4" name="work_experience"></textarea>
                                    <span class="placeholder">Աշխատանք 1</span>
									<span class="label">Աշխատանքային փորձ</span>
								</label>
							</div>
							<span class="h_border"></span>
							<div class="form_subtitle">Երազանքներ/ Ծրագրեր/ Նպատակներ</div>
							<div class="field_block">
								<label>
									<textarea rows="9" name="dreams_plans_goals"></textarea>
                                    <span class="placeholder">Մանրամասն շարադրեք</span>
									<span class="label">Երազանքներ/ Ծրագրեր/ Նպատակներ</span>
								</label>
								<span class="error_hint">պարտադիր դաշտ</span>
							</div>
						</div>
						<button type="submit" class="validate_btn primary_btn" aria-label="apply">Ուղարկել</button>
					</form>
				</div>
			</div>
 		</div>
		<?php
			include 'templates/footer.php'
		?>
		<div class="submit_popup popup_block">
			<div class="popup_inner">
				<div class="popup_container">
					<button class="icon_close popup_close" aria-label="close"></button>
					<div class="page_title">Ձեր դիմումը հաջողությամբ ուղարկված է</div>
					<span class="status_icon icon_checked"></span>
					<!-- <div class="page_title">Տեղի է ունեցել սխալ, խնդրում ենք փորձել մի փոքր ուշ</div>
					<span class="status_icon icon_close"></span> -->
				</div>
			</div>
		</div>
		<script src="js/jquery-3.6.0.min.js"></script>
		<script src="js/jquery.form-validator.js"></script>
	 	<script src="js/main.js"></script>
 	</body>
</html>