  <!DOCTYPE HTML>
<html lang="am">
 	<head>
  		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0, viewport-fit=cover"/>
		<meta name="format-detection" content="telephone=no"/>
		<meta name="description" content="Aren Mehrabyan foundation website">
  		<title>ԱՐԵՆ ՄԵՀՐԱԲՅԱՆ ՀԻՄՆԱԴՐԱՄ</title>
		<link rel="stylesheet" href="css/main.css">
		<link rel="stylesheet" href="css/about.css">
		<?php
			include 'templates/favicons.php'
		?>
 	</head>
 	<body>
 		<?php
			include 'templates/header.php'
		?>
 		<div class="content">
			<div class="page_head_block">
				<div class="head_inner">
					<div class="page_container">
						<div class="info_block">
							<h1 class="page_title">Մեր Տեսլականը</h1>
							<div class="page_description">Հայաստանում բարձրակարգ մասնագետներ պատրաստելու միջոցով ունենալ գերզարգացած ռոբոտացված աերոտիեզերական մեքենաշինական արդյունաբերություն, գիտական գաղափարների արտադրական իրականացում։</div>
						</div>
						<div class="image_block">
							<img src="images/vision_top_image.jpg" alt="" title="" width="1160" height="700"/>
						</div>
					</div>
				</div>
			</div>

			<div class="info_section">
				<div class="page_container">
					<div class="info_block">
						<div class="inner_block">
							<div class="title_block">Կրթություն</div>
							<div class="text_block">Պատրաստել մասնագետների գերզարգացած ռոբոտացված աերոտիեզերական մեքենաշինական արդյունաբերություն ունենալու համար</div>
						</div>
						<div class="inner_block">
							<div class="title_block">Գիտական միջավայր</div>
							<div class="text_block">Ապահովելով գիտական միջավայր՝ ունենալ հեռանկարային և շարունակական զարգացող ոլորտ</div>
						</div>
						<div class="inner_block">
							<div class="title_block">Ենթակառուցվածքներ</div>
							<div class="text_block">Ունենալ ենթակառուցվածքներ, որոնք կվերահսկեն ոլորտը և ժամանակի մարտահրավերները հաղթահարելով կշարունակեն զարգանալ այն</div>
						</div>
					</div>
					<div class="image_block">
						<img src="images/vision_inner_image.jpg" alt="" title="" width="570" height="590"/>
					</div>
				</div>
			</div>
	
 		</div>
		<?php
			include 'templates/footer.php'
		?>
		<script src="js/jquery-3.6.0.min.js"></script>
	 	<script src="js/main.js"></script>
 	</body>
</html>