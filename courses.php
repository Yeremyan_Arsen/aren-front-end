  <!DOCTYPE HTML>
<html lang="am">
 	<head>
  		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0, viewport-fit=cover"/>
		<meta name="format-detection" content="telephone=no"/>
		<meta name="description" content="Aren Mehrabyan foundation website">
  		<title>ԱՐԵՆ ՄԵՀՐԱԲՅԱՆ ՀԻՄՆԱԴՐԱՄ</title>
		<link rel="stylesheet" href="css/main.css">
		<link rel="stylesheet" href="css/programs.css">
		<?php
			include 'templates/favicons.php'
		?>
 	</head>
 	<body>
 		<?php
			include 'templates/header.php'
		?>
 		<div class="content">
			<div class="page_head_block">
				<div class="head_inner">
					<div class="page_container">
						<div class="info_block">
							<h1 class="page_title">Դասընթացներ</h1>
						</div>
						<div class="image_block">
							<img src="images/courses_top_image.jpg" alt="" title="" width="1160" height="700"/>
						</div>
					</div>
				</div>
			</div>

			<div class="program_purpose">
				<div class="page_container">
					<h2 class="page_title">Դասընթացները ներառում են՝</h2>
					<div class="info_block">
						<div class="include_block">Ֆիզիկա, մաթեմատիկա, գծագրական երկրաչափություն</div>
						<div class="include_block">Ծրագրավորում</div>
						<div class="include_block">Արհեստական բանականություն, մեքենայական ուսուցում,/ վիրտուալ իրականություն</div>
						<div class="include_block">Եռաչափ ճարտարագիտական մոդելավորում</div>
						<div class="include_block">Դիզայն, գրաֆիկա</div>
						<div class="include_block">Աերոտիեզերական ճարտարագիտություն</div>
						<div class="include_block">Անգլերեն</div>
					</div>
				</div>
			</div>

			<div class="courses_list">
				<div class="page_container">
					<h2 class="page_title">Մեր դասընթացները</h2>
					<ul>
						<li>
							<div class="image_block">
								<img src="images/course_image1.jpg" alt="" title="" width="380" height="280"/>
							</div>
							<div class="info_block">
								<div class="page_title">3D մոդելավորում</div>
								<a href="apply.php" class="apply_btn">Դիմել</a>
								<div class="description_block">Իրական, ժամանակակից դիզայն Դիզայնի միջոցով ինչպես փոխանցել բջջային հավելվածի հիմնական գործառույթները և ինչպես գտնել հավասարակշռություն ֆունկցիոնալության և տպավորիչ դիզայնի միջև: Բջջային ձևավորման մեջ նրբությունները և տարատեսակ փոքր դետալները ազդում ընկալման վրա․Իրական, ժամանակակից դիզայն Դիզայնի միջոցով ինչպես փոխանցել բջջային հավելվածի հիմնական գործառույթները և ինչպես գտնել հավասարակշռություն։</div>
								<a href="course_inner.php" class="inner_link">Ծանոթանալ դասընթացի ծրագրին</a>
							</div>
						</li>
						<li>
							<div class="image_block">
								<img src="images/course_image2.jpg" alt="" title="" width="380" height="280"/>
							</div>
							<div class="info_block">
								<div class="page_title">JavaScript դասընթաց</div>
								<a href="apply.php" class="apply_btn">Դիմել</a>
								<div class="description_block">Դասընթացի ընթացքում կծանոթանանք JavaScript-ին։ Ծանոթացում փաստաթղթի օբյեկտային մոդելի հետ։ Պատուհանների և ֆրեյմների կառավարում։ Օգտագործողի հետ փոխգործողություն։ Էջերում էլեմենտների ֆորմատավորում և ոճերի աստիճանական </div>
								<a href="course_inner.php" class="inner_link">Ծանոթանալ դասընթացի ծրագրին</a>
							</div>
						</li>
						<li>
							<div class="image_block">
								<img src="images/course_image3.jpg" alt="" title="" width="380" height="280"/>
							</div>
							<div class="info_block">
								<div class="page_title">Ռոբոտաշինություն</div>
								<a href="apply.php" class="apply_btn">Դիմել</a>
								<div class="description_block">Ռոբոտաշինության դասընթացը պարզապես ծանոթություն չէ ոլորտի նոր տեխնոլոգիաներին: Ուսանողները գործնական աշխատանքների միջոցով սովորում են վիզուալ ծրագրավորում, զարգացնում են ստեղծագործական և վերլուծական մտածողություն, ձեռք են բերում թիմային աշխատանքի միջոցով խնդիրների լուծման հմտություններ: Այս ամենի շնորհիվ նրանք հավաքում ու ծրագրավորում են ռոբոտներ, որոնք իրականացնում են իրենց կողմից տրված հրահանգները:</div>
								<a href="course_inner.php" class="inner_link">Ծանոթանալ դասընթացի ծրագրին</a>
							</div>
						</li>
					</ul>
				</div>
			</div>
	
 		</div>
		<?php
			include 'templates/footer.php'
		?>
		<script src="js/jquery-3.6.0.min.js"></script>
	 	<script src="js/main.js"></script>
 	</body>
</html>