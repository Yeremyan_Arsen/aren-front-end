  <!DOCTYPE HTML>
<html>
  <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0, viewport-fit=cover"/>
      <meta name="format-detection" content="telephone=no"/>
      <meta name="description" content="Aren Mehrabyan foundation website">
      <title>404</title>
      <link rel="stylesheet" href="css/main.css">
      <link rel="stylesheet" href="css/error404.css">
      <?php
      include 'templates/favicons.php'
      ?>
  </head>
 	<body>
		<div class="error_section">
			<div class="error_block">
				<div class="block_title">SORRY, PAGE NOT FOUND</div>
				<div class="block_description">The page you are looking for might have been removed had its name changed or is temporarly unavailable.</div>
				<div class="page_links">
					<a href="index.php" class="back_home">BACK TO HOME</a>
				</div>
			</div>
			<div class="image_block">
				<img src="css/images/404.png" alt="" title="" />
			</div>
		</div>
 	</body>
</html>