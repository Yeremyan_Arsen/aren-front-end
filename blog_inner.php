  <!DOCTYPE HTML>
<html lang="am">
 	<head>
  		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0, viewport-fit=cover"/>
		<meta name="format-detection" content="telephone=no"/>
		<meta name="description" content="Aren Mehrabyan foundation website">
  		<title>ԱՐԵՆ ՄԵՀՐԱԲՅԱՆ ՀԻՄՆԱԴՐԱՄ</title>
		<link rel="stylesheet" href="css/slick.css">
		<link rel="stylesheet" href="css/jquery.fancybox.css">
		<link rel="stylesheet" href="css/main.css">
		<link rel="stylesheet" href="css/blog.css">
		<?php
			include 'templates/favicons.php'
		?>
 	</head>
 	<body>
 		<?php
			include 'templates/header.php'
		?>
 		<div class="content">
			<div class="page_head_block">
				<div class="head_inner">
					<div class="page_container">
						<div class="info_block">
							<h1 class="news_title">Եռաչափ մոդելավորման եռամսյա դասընթացների առաջին փուլի շրջանավարտներն ավարտեցին դասընթացը</h1>
							<div class="date_time">
								<span>17.03.2021</span>
								<span>3 րոպե</span>
							</div>
						</div>
						<div class="image_block">
							<img src="images/news_main_image.jpg" alt="" title="" width="1160" height="700"/>
						</div>
					</div>
				</div>
			</div>
			<div class="standard_content">
				<h2>Դասընթացի ծրագիրը, արդյունքները</h2>
				Անկախ նրանից, թե ինչ եք ստեղծում` դիզայներներին և ինժեներներին անհրաժեշտ են լավագույն գործիքներն ու լուծումները հաջորդ բարձրորակ արտադրանքը մշակելու, այն շուկա ներմուծելու և հաճախորդների պահանջները բավարարելու համար: Սույն նպատակով` 2021թ-ի հոկտեմբերի 4-ին «ԱՐԵՆ ՄԵՀՐԱԲՅԱՆ» բարեգործական հիմնադրամի նախաձեռնությամբ մեկնարկեցին Եռաչափ մոդելավորման դասընթացները՝ պատերազմի մասնակից  զինծառայողների, պատերազմի ընթացքում վիրավորում ստացած և/ կամ հաշմանդամություն ունեցող անձանց, ինչպես նաև զոհված զինծառայողների ընտանիքների անդամների, այդ թվում՝ գիտական, տեխնիկական մասնագիտություններով հետաքրքրված անձանց համար:
				<img src="images/inner_image1.jpg" alt="" title="" width="720" height="540"/>
				Ավելի քան 25 տարի Solidworks-ը նախագծման և ճարտարագիտության ոլորտում վստահելի արդյունաբերական չափորոշիչ է:  Solidworks-ի եռաչափ դիզայնի և արտադրանքի մշակման ինտուիտիվ լուծումներն օգնում են հասկանալ, ստեղծել, հաստատել, հաղորդակցվել, կառավարել և փոխանցել ձեր նորարար գաղափարները՝ արտադրանքի հիանալի նախագծերի, ինչպես նաև` ապահովում են  չափազանց հզոր ֆունկցիոնալություն, որը կրճատում է արտադրանքի մշակման ժամանակը, նվազեցնում ծախսերը և բարելավում որակը:
				<img src="images/inner_image2.jpg" alt="" title="" width="720" height="540"/>
				Solidworks-ը թույլ է տալիս ստեղծել արագ և ճշգրիտ նախագիծ/դիզայն, այդ թվում՝ 3D մոդելներ և բարդ մասերի և հավաքվող մասնիկների 2D գծագրեր: 
				<br/>
				<br/>Դասընթացները ներառում են հետևյալ առարկաները՝ 
				<br/>
				<ul>
					<li>Solidworks </li>
					<li>Գծագրական  երկրաչափություն/հանրահաշիվ </li>
					<li>Անգլերեն</li>
				</ul>
				<br/>Վերոնշյալ առարկաները դասավանդում են իրենց բնագավառում հաջողությունների հասած՝ արհեստավարժ և փորձառու մասնագետները: Սույն դասընթացները իրականացվում են «ԱՐԵՆ ՄԵՀՐԱԲՅԱՆ» բարեգործական հիմնադրամի կողմից կահավորված ժամանակակից դասարաններում: Դասընթացներն ավարտելուց հետո «ԱՐԵՆ ՄԵՀՐԱԲՅԱՆ» բարեգործական հիմնադրամը մասնակիցներին կտրամադրի որակավորման համապատասխան վկայական, շրջանավարտներին կերաշխավորի աշխատանք` կախված վերջիններիս մասնագիտացումից և առաջընթացից:
				<br/>
				<br/>Վերոնշյալ առարկաները դասավանդում են իրենց բնագավառում հաջողությունների հասած՝ արհեստավարժ և փորձառու մասնագետները: Սույն դասընթացները իրականացվում են «ԱՐԵՆ ՄԵՀՐԱԲՅԱՆ» բարեգործական հիմնադրամի կողմից կահավորված ժամանակակից դասարաններում: Դասընթացներն ավարտելուց հետո «ԱՐԵՆ ՄԵՀՐԱԲՅԱՆ» բարեգործական հիմնադրամը մասնակիցներին կտրամադրի որակավորման համապատասխան վկայական, շրջանավարտներին կերաշխավորի աշխատանք` կախված վերջիններիս մասնագիտացումից և առաջընթացից:
			</div>
			<div class="images_slider">
				<div class="page_container">
					<div class="slider_list">
						<div class="slide_block">
							<a href="images/image-25.jpg" data-fancybox="gallery" class="image_block">
								<img src="images/image-25.jpg" alt="" title="" width="260" height="360"/>
								gallery image name
							</a>
						</div>
						<div class="slide_block">
							<a href="images/image-28.jpg" data-fancybox="gallery" class="image_block">
								<img src="images/image-28.jpg" alt="" title="" width="260" height="360"/>
								gallery image name
							</a>
						</div>
						<div class="slide_block">
							<a href="images/image-29.jpg" data-fancybox="gallery" class="image_block">
								<img src="images/image-29.jpg" alt="" title="" width="260" height="360"/>
								gallery image name
							</a>
						</div>
						<div class="slide_block">
							<a href="images/image-30.jpg" data-fancybox="gallery" class="image_block">
								<img src="images/image-30.jpg" alt="" title="" width="260" height="360"/>
								gallery image name
							</a>
						</div>
						<div class="slide_block">
							<a href="images/image-25.jpg" data-fancybox="gallery" class="image_block">
								<img src="images/image-25.jpg" alt="" title="" width="260" height="360"/>
								gallery image name
							</a>
						</div>
						<div class="slide_block">
							<a href="images/image-28.jpg" data-fancybox="gallery" class="image_block">
								<img src="images/image-28.jpg" alt="" title="" width="260" height="360"/>
								gallery image name
							</a>
						</div>
						<div class="slide_block">
							<a href="images/image-29.jpg" data-fancybox="gallery" class="image_block">
								<img src="images/image-29.jpg" alt="" title="" width="260" height="360"/>
								gallery image name
							</a>
						</div>
						<div class="slide_block">
							<a href="images/image-30.jpg" data-fancybox="gallery" class="image_block">
								<img src="images/image-30.jpg" alt="" title="" width="260" height="360"/>
								gallery image name
							</a>
						</div>
					</div>
				</div>
			</div>

			<div class="video_section">
				<div class="section_inner">
					<iframe width="900" height="506" src="https://www.youtube.com/embed/FC2TOswbijU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</div>
			</div>
 		</div>
		<?php
			include 'templates/footer.php'
		?>
		<script src="js/jquery-3.6.0.min.js"></script>
		<script src="js/slick.js"></script>
		<script src="js/jquery.fancybox.js"></script>
	 	<script src="js/main.js"></script>
 	</body>
</html>