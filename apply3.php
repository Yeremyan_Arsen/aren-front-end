  <!DOCTYPE HTML>
<html lang="am">
 	<head>
  		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0, viewport-fit=cover"/>
		<meta name="format-detection" content="telephone=no"/>
		<meta name="description" content="Aren Mehrabyan foundation website">
  		<title>ԱՐԵՆ ՄԵՀՐԱԲՅԱՆ ՀԻՄՆԱԴՐԱՄ</title>
		<link rel="stylesheet" href="css/select.css">
		<link rel="stylesheet" href="css/main.css">
		<link rel="stylesheet" href="css/apply.css">
		<?php
			include 'templates/favicons.php'
		?>
 	</head>
 	<body>
 		<?php
			include 'templates/header.php'
		?>
 		<div class="content">
			<div class="page_title_block">
				<div class="page_container">
					<h1 class="page_title">Դիմել դասընթացին</h1>
				</div>
			</div>
			<div class="apply_content">
				<div class="page_container">
					<ul class="apply_steps">
						<li><a class="step_block" href="apply.php">Քայլ 1</a></li>
						<li><a class="step_block" href="apply2.php">Քայլ 2</a></li>
						<li class="current_step"><a class="step_block" href="apply3.php">Քայլ 3</a></li>
						<li><a class="step_block inactive" href="apply4.php">Քայլ 4</a></li>
					</ul>
					<form class="apply_form">
						<div class="form_fields">
							<div class="form_subtitle">Կրթություն</div>
							<div class="fields_group">
								<div class="field_block">
									<label>
										<input type="text" autocomplete="off" name="school" data-validation="required"/>
                                        <span class="placeholder">Դպրոց</span>
										<span class="label">Դպրոց</span>
									</label>
									<span class="error_hint">պարտադիր դաշտ</span>
								</div>
								<div class="field_block">
									<label>
										<select name="education" data-placeholder="Կրթություն" data-validation="required">
											<option></option>
											<option value="1">Թերի միջնակարգ</option>
											<option value="2">Միջնակարգ</option>
											<option value="3">Միջին մասնագիտական</option>
											<option value="4">Թերի Բարձրագույն</option>
											<option value="5">Բարձրագույն</option>
										</select>
										<span class="label">Կրթություն</span>
									</label>
									<span class="error_hint">պարտադիր դաշտ</span>
								</div>
							</div>
							<div class="fields_group">
								<div class="field_block">
									<label>
										<input type="text" autocomplete="off" name="foreign_languages" data-validation="required"/>
                                        <span class="placeholder">Օտար լեզուների իմացություն</span>
										<span class="label">Օտար լեզուների իմացություն</span>
									</label>
									<span class="error_hint">պարտադիր դաշտ</span>
								</div>
							</div>
							<span class="h_border"></span>
							<div class="form_subtitle">Զինվորական ծառայություն</div>
							<div class="fields_group">
								<div class="field_block">
									<label>
										<input type="text" autocomplete="off" name="military_profession" data-validation="required"/>
                                        <span class="placeholder">Զինվորական մասնագիտությունը</span>
										<span class="label">Զինվորական մասնագիտությունը</span>
									</label>
									<span class="error_hint">պարտադիր դաշտ</span>
								</div>
							</div>
							<div class="fields_group">
								<div class="field_block">
									<div class="radio_group">
										<span class="label">Ռազմական գործողությունների մասնակցությունը</span>
										<label>
											<input type="radio" name="hostilities_participation" data-validation="required">
											<span class="radio_btn">Չեմ մասնակցել</span>
										</label>
										<label>
											<input type="radio" name="hostilities_participation">
											<span class="radio_btn">Մասնակցել եմ</span>
										</label>
										<span class="error_hint">Խնդրում ենք ընտրել</span>
									</div>
								</div>
							</div>
						</div>
						<button type="submit" class="validate_btn primary_btn" aria-label="apply">Շարունակել</button>
					</form>
				</div>
			</div>
 		</div>
		<?php
			include 'templates/footer.php'
		?>
		<script src="js/jquery-3.6.0.min.js"></script>
		<script src="js/select.js"></script>
		<script src="js/jquery.form-validator.js"></script>
	 	<script src="js/main.js"></script>
 	</body>
</html>