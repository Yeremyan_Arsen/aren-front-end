  <!DOCTYPE HTML>
<html lang="am">
 	<head>
  		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0, viewport-fit=cover"/>
		<meta name="format-detection" content="telephone=no"/>
		<meta name="description" content="Aren Mehrabyan foundation website">
  		<title>ԱՐԵՆ ՄԵՀՐԱԲՅԱՆ ՀԻՄՆԱԴՐԱՄ</title>
		<link rel="stylesheet" href="css/daterangepicker.css">
		<link rel="stylesheet" href="css/select.css">
		<link rel="stylesheet" href="css/main.css">
		<link rel="stylesheet" href="css/apply.css">
		<?php
			include 'templates/favicons.php'
		?>
 	</head>
 	<body>
 		<?php
			include 'templates/header.php'
		?>
 		<div class="content">
			<div class="page_title_block">
				<div class="page_container">
					<h1 class="page_title">Դիմել դասընթացին</h1>
				</div>
			</div>
			<div class="apply_content">
				<div class="page_container">
					<ul class="apply_steps">
						<li><a class="step_block" href="apply.php">Քայլ 1</a></li>
						<li class="current_step"><a class="step_block" href="apply2.php">Քայլ 2</a></li>
						<li><a class="step_block inactive" href="apply3.php">Քայլ 3</a></li>
						<li><a class="step_block inactive" href="apply4.php">Քայլ 4</a></li>
					</ul>
					<form class="apply_form">
						<div class="form_fields">
							<div class="form_subtitle">Անձնական տվյալներ</div>
							<div class="fields_group">
								<div class="field_block">
									<label>
										<input type="text" autocomplete="off" name="fullname" data-validation="required"/>
                                        <span class="placeholder">Անուն Ազգանուն Հայրանուն</span>
										<span class="label">Անուն Ազգանուն Հայրանուն</span>
									</label>
									<span class="error_hint">պարտադիր դաշտ</span>
								</div>
								<div class="field_block">
									<label class="icon_calendar">
										<input type="text" name="birthdate" readonly class="date_input"
										data-lg="am" data-format="DD.MM.YY" placeholder="Ծննդյան ամսաթիվ" data-validation="required"/>
										<span class="label">Ծննդյան ամսաթիվ</span>
									</label>
									<span class="error_hint">պարտադիր դաշտ</span>
								</div>
                                <div class="field_block">
                                    <label>
                                        <select name="education" class="asd" data-placeholder="Կրթություն" data-validation="required">
                                            <option></option>
                                            <option value="1">Թերի միջնակարգ</option>
                                            <option value="2">Միջնակարգ</option>
                                            <option value="3">Միջին մասնագիտական</option>
                                            <option value="4">Թերի Բարձրագույն</option>
                                            <option value="5">Բարձրագույն</option>
                                        </select>
                                        <span class="label">Կրթություն</span>
                                    </label>
                                    <span class="error_hint">պարտադիր դաշտ</span>
                                </div>
                                <div class="field_block">
                                    <label>
                                        <select name="education" class="asd" data-placeholder="Կրթություն" data-validation="required">
                                            <option></option>
                                            <option value="1">Թերի միջնակարգ</option>
                                            <option value="2">Միջնակարգ</option>
                                            <option value="3">Միջին մասնագիտական</option>
                                            <option value="4">Թերի Բարձրագույն</option>
                                            <option value="5">Բարձրագույն</option>
                                        </select>
                                        <span class="label">Կրթություն</span>
                                    </label>
                                    <span class="error_hint">պարտադիր դաշտ</span>
                                </div>

                                <div class="field_block">
									<label>
										<input type="text" autocomplete="off" name="residence" data-validation="required"/>
                                        <span class="placeholder">Բնակության վայր (մարզ և գյուղ/քաղաք)</span>
										<span class="label">Բնակության վայր (մարզ և գյուղ/քաղաք)</span>
									</label>
									<span class="error_hint">պարտադիր դաշտ</span>
								</div>
								<div class="field_block">
									<label>
										<input type="text" autocomplete="off" name="family_status" data-validation="required"/>
                                        <span class="placeholder">Ընտանեկան կարգավիճակ</span>
										<span class="label">Ընտանեկան կարգավիճակ</span>
									</label>
									<span class="error_hint">պարտադիր դաշտ</span>
								</div>
								<div class="field_block">
									<label>
										<input type="text" autocomplete="off" name="childrens" data-validation="required"/>
                                        <span class="placeholder">Երեխաներ</span>
										<span class="label">Երեխաներ</span>
									</label>
									<span class="error_hint">պարտադիր դաշտ</span>
								</div>
							</div>
							<div class="fields_group">
								<div class="field_block">
									<label>
										<input type="tel" name="phone_number" autocomplete="off" class="phone_number" data-validation="required" oninput="this.value=this.value.replace(/[^0-9+]/g,'');"/>
                                        <span class="placeholder">+374 (00) 00-00-00</span>
										<span class="label">Հեռախոսահամար</span>
									</label>
									<span class="error_hint">պարտադիր դաշտ</span>
								</div>
								<div class="field_block">
									<label>
										<input type="text" autocomplete="off" name="email" data-validation="email"/>
                                        <span class="placeholder">Էլ․ փոստի հասցե</span>
										<span class="label">Էլ․ փոստի հասցե</span>
									</label>
									<span class="error_hint">
										<span class="standard_hint">պարտադիր դաշտ</span>
										<span class="individual_hint">սխալ էլ. հասցե</span>
									</span>
								</div>
								<div class="field_block">
									<div class="radio_group">
										<span class="label">Սեռ</span>
										<label>
											<input type="radio" name="gender" data-validation="required">
											<span class="radio_btn">Արական</span>
										</label>
										<label>
											<input type="radio" name="gender">
											<span class="radio_btn">Իգական</span>
										</label>
										<span class="error_hint">Խնդրում ենք ընտրել սեռը</span>
									</div>
								</div>
							</div>
						</div>
						<div class="donate_block">
                            <button type="submit" class="validate_btn primary_btn" aria-label="apply">Շարունակել</button>
                            <a href="" class="btn_back secondary_btn">Հետ գնալ</a>
                        </div>
					</form>
				</div>
			</div>
 		</div>
		<?php
			include 'templates/footer.php'
		?>
		<script src="js/jquery-3.6.0.min.js"></script>
        <script src="js/imask.js"></script>
		<script src="js/moment.js"></script>
		<script src="js/daterangepicker.js"></script>
		<script src="js/select.js"></script>
		<script src="js/jquery.form-validator.js"></script>
	 	<script src="js/main.js"></script>
 	</body>
</html>