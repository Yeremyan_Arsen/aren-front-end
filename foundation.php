<!DOCTYPE HTML>
<html lang="am">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0, viewport-fit=cover"/>
    <meta name="format-detection" content="telephone=no"/>
    <meta name="description" content="Aren Mehrabyan foundation website">
    <title>ԱՐԵՆ ՄԵՀՐԱԲՅԱՆ ՀԻՄՆԱԴՐԱՄ</title>
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/cooperate.css">
    <?php
    include 'templates/favicons.php'
    ?>
</head>
<body>
<?php
include 'templates/header.php'
?>
<div class="content">
    <div class="accounts_popup foundation_block">
        <div class="foundation_inner">
            <div class="page_container">
                <div class="bank_info">Բանկի հաշվեհամարներ
                    Ամերիա բանկ
                    /General bank account
                </div>
                <div class="block_info">

                    <table class="details_table">
                        <thead></thead>
                        <tbody>
                        <tr>
                            <td>Intermediary bank</td>
                            <td>Citibank NA, New York</td>
                        </tr>
                        <tr>
                            <td>SWIFT(BIC)</td>
                            <td>CITIUS33</td>
                        </tr>
                        <tr>
                            <td>Beneficiary’s bank</td>
                            <td>AMERIA, YEREVAN</td>
                        </tr>
                        <tr>
                            <td>Beneficiary’s bank SWIFT(BIC)</td>
                            <td>ARMIAM33</td>
                        </tr>
                        <tr>
                            <td>Beneficiary’s account</td>
                            <td>See the table for dedicated Currency</td>
                        </tr>
                        <tr>
                            <td>Beneficiary’s name</td>
                            <td>Aren Mehrabyan Charitable Foundation</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="popup_middle">
                    <table>
                        <thead>
                        <tr>
                            <th>Տարադրամ / Currency</th>
                            <th>Հաշվի համար / Beneficiary’s account</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>AMD</td>
                            <td><span class="account_number">1570074565420100</span></td>
                            <td><button class="copy_btn" aria-label="copy"></button></td>
                        </tr>
                        <tr>
                            <td>EUR</td>
                            <td><span class="account_number">1570074565420146</span></td>
                            <td><button class="copy_btn" aria-label="copy"></button></td>
                        </tr>
                        <tr>
                            <td>RUB</td>
                            <td><span class="account_number">1570074565420158</span></td>
                            <td><button class="copy_btn" aria-label="copy"></button></td>
                        </tr>
                        <tr>
                            <td>USD</td>
                            <td><span class="account_number">1570074565420101</span></td>
                            <td><button class="copy_btn" aria-label="copy"></button></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
<!--                <div class="popup_bottom">-->
<!--                    <button class="secondary_btn popup_close" aria-label="cancel">Չեղարկել</button>-->
<!--                    <button class="primary_btn" aria-label="print">Տպել</button>-->
<!--                </div>-->
            </div>
        </div>
    </div>
</div>
<?php
include 'templates/footer.php'
?>

<script src="js/jquery-3.6.0.min.js"></script>
<script src="js/jquery.form-validator.js"></script>
<script src="js/main.js"></script><script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-589071e66b72346f"></script>
</body>
</html>