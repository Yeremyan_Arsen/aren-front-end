<!DOCTYPE HTML>
<html lang="am">
<head>
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0, viewport-fit=cover"/>
    <meta name="format-detection" content="telephone=no"/>
    <meta name="description" content="Aren Mehrabyan foundation website">
    <title>ԱՐԵՆ ՄԵՀՐԱԲՅԱՆ ՀԻՄՆԱԴՐԱՄ</title>
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/cooperate.css">
    <?php
    include 'templates/favicons.php'
    ?>
</head>
<body>
<?php
include 'templates/header.php'
?>
<div class="content">
    <div class="cooperate_top">
        <div class="image_block">
            <img src="images/cooperate_top_image.jpg" alt="" title="" width="1920" height="520"/>
        </div>
        <div class="info_block">
            <div class="page_container">
                <div class="info_inner switch_content" data-hash="yes">
                    <div class="info_preview">
                        <h2 class="page_title">Նվիրաբերել</h2>
                        <div class="btns_block">
                            <button class="primary_btn switch_btn" data-type="by_card" aria-label="card_method">Քարտով
                            </button>
                            <button class="primary_btn switch_btn" data-type="by_paypal" aria-label="paypal_method">
                                Paypal-ով
                            </button>
                            <!--									<a href="https://www.facebook.com" class="primary_btn switch_btn">Crypto</a>-->
                            <button class="primary_btn switch_btn" data-type="bank_transfer" aria-label="bank_transfer">
                                Bank transfer
                            </button>
                        </div>
                    </div>
                    <form class="switch_block by_card">
                        <a href="" class="switch_top">
                            <!--                            <button class="back_btn icon_left" aria-label="back"></button>-->
                            <h2 class="page_title icon_left">Քարտով նվիրատվություն</h2>
                        </a>
                        <div class="currency_btns">
                            <div class="field_name">Գումարի տեսակը</div>
                            <ul class="currency_list">
                                <li>
                                    <label>
                                        <input type="radio" name="currency_type" data-type="usd" data-value="$"
                                               data-min="10" data-max="1000"/>
                                        <span class="radio_btn">USD</span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input type="radio" name="currency_type" data-type="eur" data-value="€"
                                               data-min="10" data-max="1000"/>
                                        <span class="radio_btn">EUR</span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input type="radio" name="currency_type" data-type="amd" data-value="AMD"
                                               data-min="5000" data-max="500000" data-favorite="true" checked/>
                                        <span class="radio_btn">AMD</span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input type="radio" name="currency_type" data-type="rub" data-value="RUB"
                                               data-min="1000" data-max="100000"/>
                                        <span class="radio_btn">RUB</span>
                                    </label>
                                </li>
                            </ul>
                        </div>
                        <div class="hot_ammounts">
                            <div class="field_name">Գումարի չափը</div>
                            <ul class="ammounts_list usd">
                                <li>
                                    <label>
                                        <input type="radio" name="usd_ammount_size" data-value="25"/>
                                        <span class="radio_btn">$ 25</span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input type="radio" name="usd_ammount_size" data-value="50"/>
                                        <span class="radio_btn">$ 50</span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input type="radio" name="usd_ammount_size" data-value="100" checked
                                               data-favorite="true"/>
                                        <span class="radio_btn">$ 100</span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input type="radio" name="usd_ammount_size" data-value="250"/>
                                        <span class="radio_btn">$ 250</span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input type="radio" name="usd_ammount_size" data-value="500"/>
                                        <span class="radio_btn">$ 500</span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input type="radio" name="usd_ammount_size" data-value="1000"/>
                                        <span class="radio_btn">$ 1000</span>
                                    </label>
                                </li>

                            </ul>
                            <ul class="ammounts_list eur">
                                <li>
                                    <label>
                                        <input type="radio" name="eur_ammount_size" data-value="25"/>
                                        <span class="radio_btn">€ 25</span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input type="radio" name="eur_ammount_size" data-value="50"/>
                                        <span class="radio_btn">€ 50</span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input type="radio" name="eur_ammount_size" data-value="100" checked
                                               data-favorite="true"/>
                                        <span class="radio_btn">€ 100</span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input type="radio" name="eur_ammount_size" data-value="250"/>
                                        <span class="radio_btn">€ 250</span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input type="radio" name="eur_ammount_size" data-value="500"/>
                                        <span class="radio_btn">€ 500</span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input type="radio" name="eur_ammount_size" data-value="1000"/>
                                        <span class="radio_btn">€ 1000</span>
                                    </label>
                                </li>
                            </ul>
                            <ul class="ammounts_list amd">
                                <li>
                                    <label>
                                        <input type="radio" name="amd_ammount_size" data-value="5000"/>
                                        <span class="radio_btn">5000</span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input type="radio" name="amd_ammount_size" data-value="10000"/>
                                        <span class="radio_btn">10 000</span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input type="radio" name="amd_ammount_size" data-value="50000" checked
                                               data-favorite="true"/>
                                        <span class="radio_btn">50 000</span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input type="radio" name="amd_ammount_size" data-value="100000"/>
                                        <span class="radio_btn">100 000</span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input type="radio" name="amd_ammount_size" data-value="250000"/>
                                        <span class="radio_btn">250 000</span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input type="radio" name="amd_ammount_size" data-value="500000"/>
                                        <span class="radio_btn">500 000</span>
                                    </label>
                                </li>

                            </ul>
                            <ul class="ammounts_list rub">
                                <li>
                                    <label>
                                        <input type="radio" name="rub_ammount_size" data-value="1000"/>
                                        <span class="radio_btn">1000</span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input type="radio" name="rub_ammount_size" data-value="5000"/>
                                        <span class="radio_btn">5000</span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input type="radio" name="rub_ammount_size" data-value="10000" checked
                                               data-favorite="true"/>
                                        <span class="radio_btn">10 000</span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input type="radio" name="rub_ammount_size" data-value="25000"/>
                                        <span class="radio_btn">25 000</span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input type="radio" name="rub_ammount_size" data-value="50000"/>
                                        <span class="radio_btn">50 000</span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input type="radio" name="rub_ammount_size" data-value="100000"/>
                                        <span class="radio_btn">100 000</span>
                                    </label>
                                </li>
                            </ul>
                        </div>
                        <div class="custom_size">
                            <label>
                                <span class="field_name">Լրացնել  ցանկալի գումարը</span>
                                <input type="text" class="other_block" name="custom_ammount_size" placeholder="Օր․ 100" oninput="this.value=this.value.replace(/[^0-9]/g,'');"/>
                            </label>
                            <div class="donate_size">
                                <label>
                                    <input type="text" readonly name="donate_size" data-validation="number"
                                           data-validation-allowing="range[10;1000]">
                                    <span class="label">գումարի չափը</span>
                                </label>
                                <span class="error_hint">նվազագույնը <span
                                            class="min_size">$ 10</span>, առավելագույնը <span
                                            class="max_size">$ 1000</span></span>
                            </div>
                        </div>
                        <div class="donate_form">
                            <div class="showed_donate">
                                <span class="showed_currency">$</span>
                                <span class="showed_size"></span>
                            </div>
                            <div class="info_block">Նվիրաբերվող գումարը</div>

                            <div class="donate_currency">
                                <label>
                                    <input type="text" name="donate_currency" value="usd"/>
                                    <span class="label">փոխարժեք</span>
                                </label>
                            </div>
                            <div class="big_donate_info">
                                Եթե ցանկանում եք նվիրաբերել նշված առավելագույն գումարից ավել, խնդրում ենք ուղիղ
                                փոխանցում կատարել հետևյալ բանկային հաշվեհամարներին:
                                <br/><a href="#" class="popup_btn" data-popup="accounts_popup">Հետևեք նշված լինկին:</a>
                            </div>
                            <div class="donater_fields">
                                <div class="field_block small_field">
                                    <label>
                                        <span class="label">Անուն ազգանուն</span>
                                        <input type="text" autocomplete="off" name="name_surname" data-validation="required"/>
                                        <span class="placeholder">Անուն ազգանուն</span>
                                    </label>
                                    <span class="error_hint">պարտադիր դաշտ</span>
                                </div>
                                <div class="field_block middle_field">
                                    <label>
                                        <span class="label">Էլ. հասցե</span>
                                        <input type="text" autocomplete="off" name="email" data-validation="email"/>
                                        <span class="placeholder">Էլ. հասցե</span>
                                    </label>
                                    <span class="error_hint">
												<span class="standard_hint">պարտադիր դաշտ</span>
												<span class="individual_hint">սխալ էլ. հասցե</span>
											</span>
                                </div>
                                <div class="field_block full_field textarea">
                                    <label>
                                        <span class="label">Փեր նամակը</span>
                                        <textarea name="message" data-validation="required"></textarea>
                                        <span class="placeholder">Ձեր նամակը</span>
                                    </label>
                                    <span class="error_hint">պարտադիր դաշտ</span>
                                </div>
                                <div class="regular_switch regular_switch_custom">
                                    <div class="field_name field_name_switch">Դարձնել պարբերական</div>
                                    <div class="radio_group">
                                        <label>
                                            <input type="radio" class="qqq" name="regular">
                                            <span class="radio_btn">Շաբաթական</span>
                                        </label>
                                        <label>
                                            <input type="radio" class="qqq"name="regular">
                                            <span class="radio_btn">Ամսական</span>
                                        </label>
                                    </div>
                                </div>

                                <div class="field_block checkbox_many">
                                    <label class="checkbox_label">
                                        <input type="checkbox" name="ammount_publish"/>
                                        <span class="check_btn">Հրապարակել գումարի չափը</span>
                                    </label>
                                </div>
                                <div class="field_block checkbox_many">
                                    <label class="checkbox_label">
                                        <input type="checkbox" name="name_publish"/>
                                        <span class="check_btn">Հրապարակել անունը</span>
                                    </label>
                                </div>
                            </div>
                            <div class="form_btns">
                                <button class="secondary_btn" aria-label="cancel">Չեղարկել</button>
                                <button class="validate_btn primary_btn" aria-label="submit">Նվիրաբերել</button>
                            </div>
                        </div>
                        <div class="share_btns">
                            <div class="share_inner">
                                Կիսվել:
                                <div class="addthis_inline_share_toolbox_gyl9"></div>
                                <a href="" class="icon_files_empty copy_btn_custom"></a>
                            </div>

                            <div class="card_block">
                                <img src="css/images/card.png" title="" alt=""/>
                            </div>
                        </div>
                    </form>
                    <form class="switch_block by_paypal">
                        <a href="" class="switch_top">
                            <!--                            <button class="back_btn icon_left" aria-label="back"></button>-->
                            <h2 class="page_title icon_left">Նվիրաբերել Paypal-ով</h2>
                        </a>
                        <div class="hot_ammounts">
                            <div class="field_name">Գումարի չափը</div>
                            <ul class="ammounts_list">
                                <li>
                                    <label>
                                        <input type="radio" name="hot_ammount_size" data-value="25"/>
                                        <span class="radio_btn">$ 25</span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input type="radio" name="hot_ammount_size" data-value="50"/>
                                        <span class="radio_btn">$ 50</span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input type="radio" name="hot_ammount_size" data-value="100" checked
                                               data-favorite="true"/>
                                        <span class="radio_btn">$ 100</span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input type="radio" name="hot_ammount_size" data-value="250"/>
                                        <span class="radio_btn">$ 250</span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input type="radio" name="hot_ammount_size" data-value="500"/>
                                        <span class="radio_btn">$ 500</span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input type="radio" name="hot_ammount_size" data-value="1000"/>
                                        <span class="radio_btn">$ 1000</span>
                                    </label>
                                </li>
                            </ul>
                        </div>
                        <div class="custom_size">
                            <label>
                                <span class="field_name">Լրացնել  ցանկալի գումարը</span>
                                <input type="text" class="other_block" name="custom_ammount_size" placeholder="Օր․ 100" oninput="this.value=this.value.replace(/[^0-9]/g,'');"/>
                            </label>
                            <div class="donate_size">
                                <label>
                                    <input type="text" readonly name="donate_size" data-validation="number"
                                           data-validation-allowing="range[10;1000]">
                                    <span class="label">գումարի չափը</span>
                                </label>
                                <span class="error_hint">նվազագույնը <span
                                            class="min_size">$ 10</span>, առավելագույնը <span
                                            class="max_size">$ 1000</span></span>
                            </div>
                        </div>
                        <div class="donate_form">
                            <div class="showed_donate">
                                <span class="showed_currency">$</span>
                                <span class="showed_size"></span>
                            </div>
                            <div class="info_block">Նվիրաբերվող գումարը</div>

                            <div class="donate_currency">
                                <label>
                                    <input type="text" name="donate_currency" value="usd"/>
                                    <span class="label">փոխարժեք</span>
                                </label>
                            </div>

                            <div class="form_btns">
                                <button class="secondary_btn" aria-label="cancel">Չեղարկել</button>
                                <button class="validate_btn primary_btn" aria-label="submit">
                                    <img src="css/images/paypal_logo.svg" alt="" title="" width="101" height="32"/>
                                </button>
                            </div>
                            <div class="type_info">DEBET կամ CREDIT քարտով</div>
                        </div>
                        <div class="share_btns">
                            <div class="share_inner">
                                Կիսվել:
                                <div class="addthis_inline_share_toolbox_gyl9"></div>
                                <a href="" class="icon_files_empty copy_btn_custom"></a>
                            </div>

                            <div class="card_block">
                                <img src="css/images/card.png" title="" alt=""/>
                            </div>
                        </div>
                    </form>
                    <div class="switch_block bank_transfer">
                        <a href="" class="switch_top">
                            <!--                            <button class="back_btn icon_left" aria-label="back"></button>-->
                            <h2 class="page_title icon_left">Նվիրաբերել բանկային փոխանցում</h2>
                        </a>
                        <div class="accounts_popup foundation_block">
                            <div class="foundation_inner">
                                <!--                                <div class="bank_info">Բանկի հաշվեհամարներ-->
                                <!--                                    Ամերիա բանկ-->
                                <!--                                    /General bank account-->
                                <!--                                </div>-->
                                <div class="block_info">

                                    <table class="details_table">
                                        <thead></thead>
                                        <tbody>
                                        <tr>
                                            <td>Intermediary bank</td>
                                            <td>Citibank NA, New York <button class="copy_btn icon_files_empty" aria-label="copy"></button></td>
                                        </tr>
                                        <tr>
                                            <td>SWIFT(BIC)</td>
                                            <td>CITIUS33 <button class="copy_btn icon_files_empty" aria-label="copy"></button></td>
                                        </tr>
                                        <tr>
                                            <td>Beneficiary’s bank</td>
                                            <td>AMERIA, YEREVAN <button class="copy_btn icon_files_empty" aria-label="copy"></button></td>
                                        </tr>
                                        <tr>
                                            <td>Beneficiary’s bank SWIFT(BIC)</td>
                                            <td>ARMIAM33 <button class="copy_btn icon_files_empty" aria-label="copy"></button></td>
                                        </tr>
                                        <tr>
                                            <td>Beneficiary’s account</td>
                                            <td>See the table for dedicated Currency <button class="copy_btn icon_files_empty" aria-label="copy"></button></td>
                                        </tr>
                                        <tr>
                                            <td>Beneficiary’s name</td>
                                            <td>Aren Mehrabyan Charitable Foundation <button class="copy_btn icon_files_empty" aria-label="copy"></button></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="popup_middle">
                                    <table>
                                        <thead>
                                        <tr>
                                            <th>Տարադրամ / Currency</th>
                                            <th>Հաշվի համար / Beneficiary’s account</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>AMD</td>
                                            <td><span class="account_number">1570074565420100</span></td>
                                            <td><button class="copy_btn icon_files_empty" aria-label="copy"></button></td>
                                        </tr>
                                        <tr>
                                            <td>EUR</td>
                                            <td><span class="account_number">1570074565420146</span></td>
                                            <td><button class="copy_btn icon_files_empty" aria-label="copy"></button></td>
                                        </tr>
                                        <tr>
                                            <td>RUB</td>
                                            <td><span class="account_number">1570074565420158</span></td>
                                            <td><button class="copy_btn icon_files_empty" aria-label="copy"></button></td>
                                        </tr>
                                        <tr>
                                            <td>USD</td>
                                            <td><span class="account_number">1570074565420101</span></td>
                                            <td><button class="copy_btn icon_files_empty" aria-label="copy"></button></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="popup_bottom">
                                    <button class="secondary_btn popup_close" aria-label="cancel">Չեղարկել</button>
                                    <button class="primary_btn" aria-label="print">Տպել</button>
                                </div>
                            </div>
                        </div>
                        <div class="share_btns">
                            <div class="share_inner">
                                Կիսվել:
                                <div class="addthis_inline_share_toolbox_gyl9"></div>
                                <a href="" class="icon_files_empty copy_btn_custom"></a>
                            </div>

                            <div class="card_block">
                                <img src="css/images/card.png" title="" alt=""/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="donaters_section">
        <div class="page_container">
            <div class="section_inner">
                <h2 class="page_title">Մեր աջակիցները</h2>
                <div class="donaters_list">
                    <ul>
                        <li>
                            <div class="donater_info">
                                <span class="name_block">Արուսիկ Մարկոսյան</span>
                                <div class="description_block">Հանուն հայ զինվորների ապագայի։</div>
                            </div>
                            <div class="donate_size">
                                <span class="size_block">230</span>
                                <span class="currency_block">USD</span>
                            </div>
                        </li>
                        <li>
                            <div class="donater_info">
                                <span class="name_block">Անանուն նվիրատվություն</span>
                            </div>
                            <div class="donate_size">
                                <span class="size_block">1500</span>
                                <span class="currency_block">USD</span>
                            </div>
                        </li>
                        <li>
                            <div class="donater_info">
                                <span class="name_block">Hovhannes Badalian</span>
                                <div class="description_block">I donated in support of this campaign</div>
                            </div>
                        </li>
                        <li>
                            <div class="donater_info">
                                <span class="name_block">Ծովինար Հովակիմյան</span>
                                <div class="description_block">In memoriam of two great ladies who were very kind to me
                                    and my family.
                                </div>
                            </div>
                            <div class="donate_size">
                                <span class="size_block">230</span>
                                <span class="currency_block">USD</span>
                            </div>
                        </li>
                        <li>
                            <div class="donater_info">
                                <span class="name_block">Արուսիկ Մարկոսյան</span>
                                <div class="description_block">Հանուն հայ զինվորների ապագայի։</div>
                            </div>
                            <div class="donate_size">
                                <span class="size_block">230</span>
                                <span class="currency_block">USD</span>
                            </div>
                        </li>
                        <li>
                            <div class="donater_info">
                                <span class="name_block">Անանուն նվիրատվություն</span>
                            </div>
                            <div class="donate_size">
                                <span class="size_block">1500</span>
                                <span class="currency_block">USD</span>
                            </div>
                        </li>
                        <li>
                            <div class="donater_info">
                                <span class="name_block">Hovhannes Badalian</span>
                                <div class="description_block">I donated in support of this campaign</div>
                            </div>
                        </li>
                        <li>
                            <div class="donater_info">
                                <span class="name_block">Ծովինար Հովակիմյան</span>
                                <div class="description_block">In memoriam of two great ladies who were very kind to me
                                    and my family.
                                </div>
                            </div>
                            <div class="donate_size">
                                <span class="size_block">230</span>
                                <span class="currency_block">USD</span>
                            </div>
                        </li>
                        <li>
                            <div class="donater_info">
                                <span class="name_block">Արուսիկ Մարկոսյան</span>
                                <div class="description_block">Հանուն հայ զինվորների ապագայի։</div>
                            </div>
                            <div class="donate_size">
                                <span class="size_block">230</span>
                                <span class="currency_block">USD</span>
                            </div>
                        </li>
                        <li>
                            <div class="donater_info">
                                <span class="name_block">Անանուն նվիրատվություն</span>
                            </div>
                            <div class="donate_size">
                                <span class="size_block">1500</span>
                                <span class="currency_block">USD</span>
                            </div>
                        </li>
                        <li>
                            <div class="donater_info">
                                <span class="name_block">Hovhannes Badalian</span>
                                <div class="description_block">I donated in support of this campaign</div>
                            </div>
                        </li>
                        <li>
                            <div class="donater_info">
                                <span class="name_block">Ծովինար Հովակիմյան</span>
                                <div class="description_block">In memoriam of two great ladies who were very kind to me
                                    and my family.
                                </div>
                            </div>
                            <div class="donate_size">
                                <span class="size_block">230</span>
                                <span class="currency_block">USD</span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include 'templates/footer.php'
?>
<div class="submit_popup popup_block">
    <div class="popup_inner">
        <div class="popup_container">
            <button class="icon_close popup_close" aria-label="close"></button>
            <div class="thanks_title">Շնորհակալություն</div>
            <div class="thanks_subtitle">նվիրատվության համար</div>
            <div class="icon_block">
                <img src="css/images/heart_in_hands_color.svg" alt="" title="" width="132" height="109"/>
            </div>
            <div class="description_block">Շնորհակալ ենք Ձեր 200$ գումարի չափով նվիրատվությամբ «Արեն Մեհրաբյան»
                բարեգործական հիմնադրամի կրթական և գիտական ծրագրերին աջակցելու համար։
            </div>
        </div>
    </div>
</div>

<div class="accounts_popup popup_block">
    <div class="popup_inner">
        <div class="popup_container">
            <button class="popup_close icon_close" aria-label="icon_close"></button>
            <div class="popup_top">
                <h2 class="page_title">Արեն Մեհրաբյան բարեգործական հիմնադրամ</h2>
                <div class="bank_info">Բանկի հաշվեհամարներ
                    <br/>Ամերիա բանկ
                    <br/>General bank account
                </div>
                <table class="details_table">
                    <thead></thead>
                    <tbody>
                    <tr>
                        <td>Intermediary bank</td>
                        <td>Citibank NA, New York</td>
                    </tr>
                    <tr>
                        <td>SWIFT(BIC)</td>
                        <td>CITIUS33</td>
                    </tr>
                    <tr>
                        <td>Beneficiary’s bank</td>
                        <td>AMERIA, YEREVAN</td>
                    </tr>
                    <tr>
                        <td>Beneficiary’s bank SWIFT(BIC)</td>
                        <td>ARMIAM33</td>
                    </tr>
                    <tr>
                        <td>Beneficiary’s account</td>
                        <td>See the table for dedicated Currency</td>
                    </tr>
                    <tr>
                        <td>Beneficiary’s name</td>
                        <td>Aren Mehrabyan Charitable Foundation</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="popup_middle">
                <table>
                    <thead>
                    <tr>
                        <th>Տարադրամ / Currency</th>
                        <th>Հաշվի համար / Beneficiary’s account</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>AMD</td>
                        <td><span class="account_number">1570074565420100</span></td>
                        <td>
                            <button class="copy_btn" aria-label="copy"></button>
                        </td>
                    </tr>
                    <tr>
                        <td>EUR</td>
                        <td><span class="account_number">1570074565420146</span></td>
                        <td>
                            <button class="copy_btn" aria-label="copy"></button>
                        </td>
                    </tr>
                    <tr>
                        <td>RUB</td>
                        <td><span class="account_number">1570074565420158</span></td>
                        <td>
                            <button class="copy_btn" aria-label="copy"></button>
                        </td>
                    </tr>
                    <tr>
                        <td>USD</td>
                        <td><span class="account_number">1570074565420101</span></td>
                        <td>
                            <button class="copy_btn" aria-label="copy"></button>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="popup_bottom">
                <button class="secondary_btn popup_close" aria-label="cancel">Չեղարկել</button>
                <button class="primary_btn" aria-label="print">Տպել</button>
            </div>
        </div>
    </div>
</div>
<script src="js/jquery-3.6.0.min.js"></script>
<script src="js/jquery.form-validator.js"></script>
<script src="js/main.js"></script>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-589071e66b72346f"></script>
</body>
</html>